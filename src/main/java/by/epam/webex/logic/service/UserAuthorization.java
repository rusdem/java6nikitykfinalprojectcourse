package by.epam.webex.logic.service;

import by.epam.webex.domain.User;
import by.epam.webex.logic.command.RequestParameterName;

public class UserAuthorization {
    public static final Object checkUser(String login, String password) {
        User somebody = new User(login, new Integer(password.hashCode()).toString());
        String role = DaoAccess.userDao.checkRole(somebody);
        Object user = null;
        if (role != null) {
            if (role.equals(RequestParameterName.STUDENT)) {
                user = DaoAccess.studentDao.select(DaoAccess.userDao.getId(login));
            } else if (role.equals(RequestParameterName.TEACHER)) {
                user = DaoAccess.teacherDao.select(DaoAccess.userDao.getId(login));
            } else if (role.equals(RequestParameterName.ADMIN)) {
                user = DaoAccess.userDao.select(DaoAccess.userDao.getId(login));
            }
        }
        return user;
    }
}
