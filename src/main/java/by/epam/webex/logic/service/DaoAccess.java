package by.epam.webex.logic.service;

import by.epam.webex.dao.impl.*;
import by.epam.webex.dao.join_script.JoinScriptDao;

/*
Класс для доступа в классам базы данных.
 */
public class DaoAccess {
    public static final UserDaoImpl userDao = new UserDaoImpl();
    public static final StudentDaoImpl studentDao = new StudentDaoImpl();
    public static final TeacherDaoImpl teacherDao = new TeacherDaoImpl();
    public static final CourseDaoImpl courseDao = new CourseDaoImpl();
    public static final StudentOnCourseDaoImpl studentOnCourseDao = new StudentOnCourseDaoImpl();
    public static final JoinScriptDao joinScriptDao = new JoinScriptDao();
    private DaoAccess()
    {

    }
}
