package by.epam.webex.logic.service;

import by.epam.webex.domain.User;

public class UserRegistration {
    public static boolean registration(String login, String password, String role){
        return DaoAccess.userDao.add(new User(login, password, role));
    }
}
