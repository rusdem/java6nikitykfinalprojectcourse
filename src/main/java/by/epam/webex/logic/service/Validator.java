package by.epam.webex.logic.service;

import by.epam.webex.logic.resource_manager.PropertyManager;

import java.sql.Date;
import java.util.regex.Pattern;
/*
Класс для проведения валидации с загрузкой паттернов и файла свойств
 */
public class Validator {
    private static final Pattern logPassPattern = Pattern.compile(new PropertyManager("regex").getValue("logPassPattern.regexp"));
    private static final Pattern namePattern = Pattern.compile(new PropertyManager("regex").getValue("namePattern.regexp"));
    private static final Pattern textPattern = Pattern.compile(new PropertyManager("regex").getValue("textPattern.regexp"));

    public static boolean userValidate(String surname, String name, String education) {
        if (name != null && surname != null && education != null)
            if (namePattern.matcher(name).matches()) {
                if (namePattern.matcher(surname).matches()) {
                    if (textPattern.matcher(education).matches()) {
                        return true;
                    }
                }
            }
        return false;
    }

    public static boolean courseValidate(String name, Integer lectorId, Date startDate, Date endDate) {
        if (name != null && lectorId != null && startDate != null && endDate != null)
            if (textPattern.matcher(name).matches()) {
                if (DaoAccess.teacherDao.isExist(lectorId) != -1) {
                    if (startDate.before(endDate)) {
                        return true;
                    }
                }
            }
        return false;
    }

    public static boolean logpassValidate(String login, String password) {
        if (login != null && password != null)
            if (logPassPattern.matcher(login).matches() && logPassPattern.matcher(password).matches()) {
                if (DaoAccess.userDao.isExist(login) != 1) {
                    return true;
                }
            }
        return false;
    }
}
