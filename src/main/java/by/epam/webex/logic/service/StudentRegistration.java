package by.epam.webex.logic.service;

import by.epam.webex.domain.Student;
import by.epam.webex.logic.command.RequestParameterName;

public class StudentRegistration {
    public static boolean registration(String login, String password, Student student) {
        UserRegistration.registration(login, password, RequestParameterName.STUDENT);
        student.setId(DaoAccess.userDao.getId(login));
        if (student.getId() == -1)
            return false;
        return DaoAccess.studentDao.add(student);
    }
}
