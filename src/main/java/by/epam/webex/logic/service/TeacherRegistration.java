package by.epam.webex.logic.service;

import by.epam.webex.domain.Teacher;
import by.epam.webex.logic.command.RequestParameterName;

public class TeacherRegistration {
    public static boolean registration(String login, String password, Teacher teacher) {
        UserRegistration.registration(login, password, RequestParameterName.TEACHER);
        teacher.setId(DaoAccess.userDao.getId(login));
        if (teacher.getId() == -1)
            return false;
        return DaoAccess.teacherDao.add(teacher);
    }
}
