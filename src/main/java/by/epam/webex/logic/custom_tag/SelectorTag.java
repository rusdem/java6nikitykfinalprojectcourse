package by.epam.webex.logic.custom_tag;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
/*
����� ���� ��� �������� ����������� ������ �� 2 �������� ������ �����(1 ��������, 2 �����), � ������� �����
������� ��������.
 */
public class SelectorTag extends TagSupport {
    private Object[] view;
    private Object[] value;
    private String name;

    public void setView(Object[] view) {
        this.view = view;
    }

    public void setValue(Object[] value) {
        this.value = value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int doStartTag() throws JspTagException {
        try {
            pageContext.getOut().write("<select name=\"" + name + "\" class=\"form-control\">");
            if (value.length == view.length) {
                for (int i = 0; i < value.length; i++) {
                    pageContext.getOut().write("<option value=\"" + value[i] + "\">" + view[i] + "</option>");
                }
            }
            pageContext.getOut().write("</select>");
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return SKIP_BODY;
    }
}
