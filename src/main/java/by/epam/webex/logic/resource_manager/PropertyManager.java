package by.epam.webex.logic.resource_manager;
import java.util.Locale;
import java.util.ResourceBundle;

/*
����� ��� ��������� ���������� �� ������ �������.
 */
public class PropertyManager {
    private final static String LOCALE_RESOURCE = "resources.localization.local";
    private static ResourceBundle bundle;

    public PropertyManager(Locale locale) {
        bundle = ResourceBundle.getBundle(LOCALE_RESOURCE, locale);
    }

    public PropertyManager(String propertySource){
        bundle = ResourceBundle.getBundle(propertySource);
    }

    public String getValue(String key) {
        return bundle.getString(key);
    }
}