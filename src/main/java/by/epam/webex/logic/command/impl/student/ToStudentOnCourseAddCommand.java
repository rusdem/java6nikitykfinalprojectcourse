package by.epam.webex.logic.command.impl.student;

import by.epam.webex.controller.JspPageName;
import by.epam.webex.domain.Student;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.service.DaoAccess;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ToStudentOnCourseAddCommand implements ICommand {
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            Student student = (Student) request.getSession().getAttribute(RequestParameterName.USER);
        }catch (ClassCastException e)
        {
            request.setAttribute(RequestParameterName.COMMAND_ERROR,"Class cast error");
            return JspPageName.ERROR_PAGE;
        }
        int authorizedId = (Integer) request.getSession().getAttribute(RequestParameterName.AUTHORIZED_ID);
        Integer studentId = Integer.valueOf(request.getParameter(RequestParameterName.ID));
        if (studentId == authorizedId) {
            request.setAttribute(RequestParameterName.STUDENT_NOT_ON_COURSE_LIST, DaoAccess.joinScriptDao.selectStudentCourseExceptStudentId(studentId));
            return JspPageName.STUDENT_ON_COURSE_ADD_PAGE;
        }
        request.setAttribute(RequestParameterName.ACCESS_ERROR, 1);
        return JspPageName.ERROR_PAGE;
    }
}
