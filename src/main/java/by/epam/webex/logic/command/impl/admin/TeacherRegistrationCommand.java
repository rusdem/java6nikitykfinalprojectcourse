package by.epam.webex.logic.command.impl.admin;

import by.epam.webex.controller.JspPageName;
import by.epam.webex.domain.Teacher;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.service.TeacherRegistration;
import by.epam.webex.logic.service.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TeacherRegistrationCommand implements ICommand {
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        if (request.getSession().getAttribute(RequestParameterName.ROLE) != null) {
            if (request.getSession().getAttribute(RequestParameterName.ROLE).equals(RequestParameterName.ADMIN)) {
                String login = request.getParameter(RequestParameterName.LOGIN);
                String password = request.getParameter(RequestParameterName.PASSWORD);
                if (Validator.logpassValidate(login, password)) {
                    TeacherRegistration.registration(login, password, new Teacher(null, null, null));
                    return JspPageName.ADMIN_PAGE;
                } else {
                    request.setAttribute(RequestParameterName.DATA_ERROR, 1);
                    return JspPageName.ADMIN_PAGE;
                }
            }
        }
        request.setAttribute(RequestParameterName.ACCESS_ERROR, 1);
        return JspPageName.ERROR_PAGE;
    }
}
