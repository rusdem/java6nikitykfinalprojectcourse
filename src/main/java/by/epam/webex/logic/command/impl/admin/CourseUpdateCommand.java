package by.epam.webex.logic.command.impl.admin;

import by.epam.webex.controller.JspPageName;
import by.epam.webex.domain.Course;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.service.DaoAccess;
import by.epam.webex.logic.service.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Date;

public class CourseUpdateCommand implements ICommand {
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        if (request.getSession().getAttribute(RequestParameterName.ROLE) != null) {
            if (request.getSession().getAttribute(RequestParameterName.ROLE).equals(RequestParameterName.ADMIN)) {
                String name = request.getParameter(RequestParameterName.NAME);
                Integer id = null;
                Integer lector = null;
                Date startDate = null;
                Date endDate = null;
                try {
                    lector = Integer.valueOf(request.getParameter(RequestParameterName.LECTOR));
                    startDate = Date.valueOf(request.getParameter(RequestParameterName.START_DATE));
                    endDate = Date.valueOf(request.getParameter(RequestParameterName.END_DATE));
                    id = Integer.valueOf(request.getParameter(RequestParameterName.ID));
                } catch (IllegalArgumentException e) {

                }
                if (Validator.courseValidate(name, lector, startDate, endDate) && id != null) {
                    DaoAccess.courseDao.update(new Course(id, lector, name, startDate, endDate));
                    Date min;
                    Date max;
                    try {
                        min = (Date)request.getSession().getAttribute(RequestParameterName.MIN_DATE);
                    } catch (IllegalArgumentException e) {
                        System.out.println(request.getSession().getAttribute(RequestParameterName.MIN_DATE));
                        min = null;
                    }
                    try {
                        max = (Date)request.getSession().getAttribute(RequestParameterName.MAX_DATE);
                    } catch (IllegalArgumentException e) {
                        System.out.println(request.getSession().getAttribute(RequestParameterName.MAX_DATE));
                        max = null;
                    }
                    if (min == null && max == null) {
                        request.setAttribute(RequestParameterName.COURSE_LIST, DaoAccess.courseDao.selectAll());
                    } else {
                        if (max == null) {
                            max = new Date(min.getTime());
                            max.setYear(min.getYear() + 1);
                        } else if (min == null) {
                            min = new Date(max.getTime());
                            min.setYear(max.getYear() - 1);
                        }
                        request.setAttribute(RequestParameterName.COURSE_LIST, DaoAccess.courseDao.selectByDate(min, max));
                    }
                    request.setAttribute(RequestParameterName.TEACHER_LIST, DaoAccess.teacherDao.selectAll());
                    return JspPageName.COURSE_EDIT_PAGE;
                } else {
                    request.setAttribute(RequestParameterName.DATA_ERROR, 1);
                    return JspPageName.COURSE_EDIT_PAGE;
                }
            }
        }
        request.setAttribute(RequestParameterName.ACCESS_ERROR, 1);
        return JspPageName.ERROR_PAGE;
    }
}
