package by.epam.webex.logic.command.impl.common;

import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RedirectToCommand implements ICommand{
    private final String pageUrl;

    public RedirectToCommand(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        return pageUrl;
    }
}
