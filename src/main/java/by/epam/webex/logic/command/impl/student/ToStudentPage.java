package by.epam.webex.logic.command.impl.student;

import by.epam.webex.controller.JspPageName;
import by.epam.webex.domain.Student;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.service.DaoAccess;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ToStudentPage implements ICommand {

    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        int authorizedId;
        int userId;
        try {
            authorizedId = (Integer) request.getSession().getAttribute(RequestParameterName.AUTHORIZED_ID);
            userId = ((Student) request.getSession().getAttribute(RequestParameterName.USER)).getId();
            if (userId == authorizedId) {
                request.setAttribute(RequestParameterName.STUDENT_ON_COURSE_LIST,DaoAccess.joinScriptDao.selectStudentCourseByStudent(userId));
                return JspPageName.STUDENT_PAGE;
            }
        } catch (ClassCastException e) {
            request.setAttribute(RequestParameterName.COMMAND_ERROR, "Class Cast Error");
        }
        request.setAttribute(RequestParameterName.ACCESS_ERROR, 1);
        return JspPageName.ERROR_PAGE;
    }
}
