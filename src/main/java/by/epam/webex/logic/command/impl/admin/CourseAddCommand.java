package by.epam.webex.logic.command.impl.admin;

import by.epam.webex.controller.JspPageName;
import by.epam.webex.domain.Course;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.service.DaoAccess;
import by.epam.webex.logic.service.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Date;

public class CourseAddCommand implements ICommand {
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        if (request.getSession().getAttribute(RequestParameterName.ROLE) != null) {
            if (request.getSession().getAttribute(RequestParameterName.ROLE).equals(RequestParameterName.ADMIN)) {
                String name = request.getParameter(RequestParameterName.NAME);
                Integer lector = Integer.valueOf(request.getParameter(RequestParameterName.LECTOR));
                Date startDate = Date.valueOf(request.getParameter(RequestParameterName.START_DATE));
                Date endDate = Date.valueOf(request.getParameter(RequestParameterName.END_DATE));
                if (Validator.courseValidate(name,lector,startDate,endDate)) {
                    DaoAccess.courseDao.add(new Course(lector,name,startDate,endDate));
                    return JspPageName.ADMIN_PAGE;
                } else {
                    request.setAttribute(RequestParameterName.DATA_ERROR, 1);
                    return JspPageName.ADMIN_PAGE;
                }
            }
        }
        request.setAttribute(RequestParameterName.ACCESS_ERROR, 1);
        return JspPageName.ERROR_PAGE;
    }
}
