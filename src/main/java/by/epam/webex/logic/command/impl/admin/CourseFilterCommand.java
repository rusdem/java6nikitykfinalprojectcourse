package by.epam.webex.logic.command.impl.admin;

import by.epam.webex.controller.JspPageName;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.service.DaoAccess;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Date;

public class CourseFilterCommand implements ICommand {
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        if (request.getSession().getAttribute(RequestParameterName.ROLE) != null) {
            if (request.getSession().getAttribute(RequestParameterName.ROLE).equals(RequestParameterName.ADMIN)) {
                request.setAttribute(RequestParameterName.TEACHER_LIST, DaoAccess.teacherDao.selectAll());
                Date min;
                Date max;
                try {
                    min = Date.valueOf(request.getParameter(RequestParameterName.MIN_DATE));
                } catch (IllegalArgumentException e) {
                    System.out.println(request.getParameter(RequestParameterName.MIN_DATE));
                    min = null;
                }
                try {
                    max = Date.valueOf(request.getParameter(RequestParameterName.MAX_DATE));
                } catch (IllegalArgumentException e) {
                    System.out.println(request.getParameter(RequestParameterName.MAX_DATE));
                    max = null;
                }
                if (min == null && max == null) {
                    request.setAttribute(RequestParameterName.MIN_DATE, min);
                    request.setAttribute(RequestParameterName.MAX_DATE, max);
                    request.setAttribute(RequestParameterName.COURSE_LIST, DaoAccess.courseDao.selectAll());
                } else {
                    if (max == null) {
                        max = new Date(min.getTime());
                        max.setYear(min.getYear() + 1);
                    } else if (min == null) {
                        min = new Date(max.getTime());
                        min.setYear(max.getYear() - 1);
                    }
                    request.setAttribute(RequestParameterName.MIN_DATE, min);
                    request.setAttribute(RequestParameterName.MAX_DATE, max);
                    request.setAttribute(RequestParameterName.COURSE_LIST, DaoAccess.courseDao.selectByDate(min, max));
                }
                return JspPageName.COURSE_EDIT_PAGE;
            }
        }
        request.setAttribute(RequestParameterName.ACCESS_ERROR, 1);
        return JspPageName.ERROR_PAGE;
    }
}
