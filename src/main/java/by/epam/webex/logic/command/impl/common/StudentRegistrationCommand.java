package by.epam.webex.logic.command.impl.common;

import by.epam.webex.controller.JspPageName;
import by.epam.webex.domain.Student;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.service.StudentRegistration;
import by.epam.webex.logic.service.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StudentRegistrationCommand implements ICommand{

    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String login = request.getParameter(RequestParameterName.LOGIN);
        String password = request.getParameter(RequestParameterName.PASSWORD);
        if(!Validator.logpassValidate(login, password)) {
            request.setAttribute(RequestParameterName.LOGIN_ERROR,1);
            return JspPageName.STUDENT_REGISTRATION_PAGE;
        }
        String name = request.getParameter(RequestParameterName.NAME);
        String surname = request.getParameter(RequestParameterName.SURNAME);
        String education = request.getParameter(RequestParameterName.EDUCATION);
        if(Validator.userValidate(surname, name, education)){
            StudentRegistration.registration(login,password,new Student(surname,name,education));
            return JspPageName.INDEX_PAGE;
        }
        request.setAttribute(RequestParameterName.DATA_ERROR,1);
        return JspPageName.STUDENT_REGISTRATION_PAGE;
    }
}
