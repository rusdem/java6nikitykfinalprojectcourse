package by.epam.webex.logic.command.impl.common;

import by.epam.webex.controller.JspPageName;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogoutCommand implements ICommand {
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        request.getSession().setAttribute(RequestParameterName.ROLE, null);
        request.getSession().setAttribute(RequestParameterName.AUTHORIZED_ID,null);
        request.getSession().setAttribute(RequestParameterName.USER, null);
        return JspPageName.INDEX_PAGE;
    }
}
