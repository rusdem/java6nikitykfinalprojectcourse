package by.epam.webex.logic.command.impl.common;

import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ChangeLocaleCommand implements ICommand {
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String lang = request.getParameter(RequestParameterName.LOCAL_NAME);
        request.getSession().setAttribute(RequestParameterName.LOCALIZATION_NAME,lang);
        return null;
    }
}
