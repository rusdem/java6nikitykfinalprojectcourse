package by.epam.webex.logic.command.impl.student;


import by.epam.webex.controller.JspPageName;
import by.epam.webex.domain.Student;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.service.DaoAccess;
import by.epam.webex.logic.service.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StudentUpdateCommand implements ICommand {
    private ToStudentPage toStudentPage = new ToStudentPage();
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        Student student = null;
        int authorizedId = (Integer) request.getSession().getAttribute(RequestParameterName.AUTHORIZED_ID);
        Integer studentId = Integer.valueOf(request.getParameter(RequestParameterName.ID));
        if (studentId == authorizedId) {
            String name = request.getParameter(RequestParameterName.NAME);
            String surname = request.getParameter(RequestParameterName.SURNAME);
            String education = request.getParameter(RequestParameterName.EDUCATION);
            if (Validator.userValidate(surname, name, education)) {
                DaoAccess.studentDao.update(student = new Student(authorizedId, surname, name, education));
                request.getSession().setAttribute(RequestParameterName.USER,student);
                request.setAttribute(RequestParameterName.STUDENT, student);
                return toStudentPage.execute(request,response);
            }
            request.setAttribute(RequestParameterName.DATA_ERROR, 1);
            return JspPageName.STUDENT_EDIT_PAGE;
        }
        request.setAttribute(RequestParameterName.ACCESS_ERROR, 1);
        return JspPageName.ERROR_PAGE;
    }
}
