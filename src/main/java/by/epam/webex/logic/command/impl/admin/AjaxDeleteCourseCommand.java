package by.epam.webex.logic.command.impl.admin;

import by.epam.webex.domain.Course;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.service.DaoAccess;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AjaxDeleteCourseCommand implements ICommand {
    private static final String JSON = "json";

    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        if (request.getSession().getAttribute(RequestParameterName.ROLE) != null) {
            if (request.getSession().getAttribute(RequestParameterName.ROLE).equals(RequestParameterName.ADMIN)) {
                String json = (String) request.getAttribute(JSON);
                ObjectMapper mapper = new ObjectMapper();
                try {
                    Course course = mapper.readValue(json, Course.class);
                    DaoAccess.courseDao.delete(course.getId());
                    mapper.writeValue(response.getOutputStream(), true);
                } catch (IOException e) {
                    throw new CommandException(e);
                }
            }
        }
        request.setAttribute(RequestParameterName.ACCESS_ERROR, 1);
        return null;
    }
}
