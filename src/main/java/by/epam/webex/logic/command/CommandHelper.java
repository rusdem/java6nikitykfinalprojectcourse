package by.epam.webex.logic.command;

import by.epam.webex.controller.JspPageName;
import by.epam.webex.logic.command.impl.admin.*;
import by.epam.webex.logic.command.impl.common.*;
import by.epam.webex.logic.command.impl.student.StudentOnCourseAddCommand;
import by.epam.webex.logic.command.impl.student.StudentOnCourseDeleteCommand;
import by.epam.webex.logic.command.impl.student.StudentUpdateCommand;
import by.epam.webex.logic.command.impl.student.ToStudentOnCourseAddCommand;
import by.epam.webex.logic.command.impl.teacher.StudentOnCourseUpdateCommand;
import by.epam.webex.logic.command.impl.teacher.TeacherCourseFilterCommand;
import by.epam.webex.logic.command.impl.teacher.TeacherUpdateCommand;
import by.epam.webex.logic.command.impl.teacher.ToTeacherEditCourseCommand;

import java.util.HashMap;
import java.util.Map;

public final class CommandHelper {
    private static final CommandHelper instance = new CommandHelper();

    private Map<CommandName, ICommand> commands = new HashMap<CommandName, ICommand>();

    public CommandHelper() {
        commands.put(CommandName.AUTHENTICATION_COMMAND, new AuthorizationCommand());
        commands.put(CommandName.LOGOUT_COMMAND, new LogoutCommand());
        commands.put(CommandName.CHANGE_LOCALE_COMMAND, new ChangeLocaleCommand());
        commands.put(CommandName.NO_SUCH_COMMAND, new NoSuchCommand());
        commands.put(CommandName.STUDENT_REGISTRATION_COMMAND, new StudentRegistrationCommand());
        commands.put(CommandName.STUDENT_UPDATE_COMMAND, new StudentUpdateCommand());
        commands.put(CommandName.STUDENT_ON_COURSE_ADD_COMMAND, new StudentOnCourseAddCommand());
        commands.put(CommandName.STUDENT_ON_COURSE_DELETE_COMMAND, new StudentOnCourseDeleteCommand());
        commands.put(CommandName.STUDENT_ON_COURSE_UPDATE_COMMAND, new StudentOnCourseUpdateCommand());
        commands.put(CommandName.TO_STUDENT_ON_COURSE_ADD_COMMAND, new ToStudentOnCourseAddCommand());
        commands.put(CommandName.ADMIN_REGISTRATION_COMMAND, new AdminRegistrationCommand());
        commands.put(CommandName.TO_COURSE_ADD_COMMAND, new ToCourseAddCommand());
        commands.put(CommandName.COURSE_ADD_COMMAND, new CourseAddCommand());
        commands.put(CommandName.COURSE_FILTER_COMMAND, new CourseFilterCommand());
        commands.put(CommandName.COURSE_DELETE_COMMAND, new AjaxDeleteCourseCommand());
        commands.put(CommandName.COURSE_UPDATE_COMMAND, new CourseUpdateCommand());
        commands.put(CommandName.TEACHER_REGISTRATION_COMMAND, new TeacherRegistrationCommand());
        commands.put(CommandName.TEACHER_UPDATE_COMMAND, new TeacherUpdateCommand());
        commands.put(CommandName.TO_TEACHER_EDIT_COURSE_COMMAND, new ToTeacherEditCourseCommand());
        commands.put(CommandName.TEACHER_COURSE_FILTER_COMMAND, new TeacherCourseFilterCommand());
        commands.put(CommandName.REDIRECT_TO_REGISTRATION_COMMAND, new RedirectToCommand(JspPageName.STUDENT_REGISTRATION_PAGE));
        commands.put(CommandName.REDIRECT_TO_INDEX_COMMAND, new RedirectToCommand(JspPageName.INDEX_PAGE));
        commands.put(CommandName.REDIRECT_TO_STUDENT_COMMAND, new RedirectToCommand(JspPageName.STUDENT_PAGE));
        commands.put(CommandName.REDIRECT_TO_STUDENT_EDIT_COMMAND, new RedirectToCommand(JspPageName.STUDENT_EDIT_PAGE));
        commands.put(CommandName.REDIRECT_TO_TEACHER_COMMAND, new RedirectToCommand(JspPageName.TEACHER_PAGE));
        commands.put(CommandName.REDIRECT_TO_TEACHER_EDIT_COMMAND, new RedirectToCommand(JspPageName.TEACHER_EDIT_PAGE));
        commands.put(CommandName.REDIRECT_TO_COURSE_EDIT_COMMAND, new RedirectToCommand(JspPageName.COURSE_EDIT_PAGE));
        commands.put(CommandName.REDIRECT_TO_COURSE_ADD_COMMAND, new RedirectToCommand(JspPageName.COURSE_ADD_PAGE));
        commands.put(CommandName.REDIRECT_TO_ADMIN_PAGE_COMMAND, new RedirectToCommand(JspPageName.ADMIN_PAGE));
        commands.put(CommandName.TO_MAIN_PAGE_COMMAND, new ToMainPageCommand());

    }

    public static CommandHelper getInstance() {
        return instance;
    }

    public ICommand getCommand(String commandName) {
        ICommand command;
        try {
            CommandName name = CommandName.valueOf(commandName.toUpperCase());
            command = commands.get(name);
        }catch (IllegalArgumentException e)
        {
            command = commands.get(CommandName.NO_SUCH_COMMAND);
        }
        return command;
    }
} 