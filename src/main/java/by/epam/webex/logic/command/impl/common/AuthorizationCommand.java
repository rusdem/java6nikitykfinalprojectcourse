package by.epam.webex.logic.command.impl.common;

import by.epam.webex.controller.JspPageName;
import by.epam.webex.domain.Student;
import by.epam.webex.domain.Teacher;
import by.epam.webex.domain.User;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.command.impl.student.ToStudentPage;
import by.epam.webex.logic.service.UserAuthorization;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthorizationCommand implements ICommand {
    private ToStudentPage toStudentPage = new ToStudentPage();
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        request.getSession().removeAttribute(RequestParameterName.ROLE);
        request.getSession().removeAttribute(RequestParameterName.USER);
        request.getSession().removeAttribute(RequestParameterName.AUTHORIZED_ID);
        String login = request.getParameter(RequestParameterName.LOGIN);
        String password = request.getParameter(RequestParameterName.PASSWORD);
        Object user = UserAuthorization.checkUser(login, password);
        if(user==null)
        {
            request.setAttribute(RequestParameterName.AUTHORIZATION_ERROR, 1);
            return JspPageName.INDEX_PAGE;
        }
        else if (user.getClass().equals(Student.class)) {
            request.getSession().setAttribute(RequestParameterName.ROLE, RequestParameterName.STUDENT);
            Student student = (Student) user;
            request.setAttribute(RequestParameterName.STUDENT, student);
            request.getSession().setAttribute(RequestParameterName.USER, student);
            request.getSession().setAttribute(RequestParameterName.AUTHORIZED_ID, student.getId());
            return toStudentPage.execute(request,response);
        } else if (user.getClass().equals(Teacher.class)) {
            request.getSession().setAttribute(RequestParameterName.ROLE, RequestParameterName.TEACHER);
            Teacher teacher = (Teacher) user;
            request.setAttribute(RequestParameterName.TEACHER, teacher);
            request.getSession().setAttribute(RequestParameterName.USER, teacher);
            request.getSession().setAttribute(RequestParameterName.AUTHORIZED_ID, teacher.getId());
            return JspPageName.TEACHER_PAGE;
        } else if (user.getClass().equals(User.class)) {
            request.getSession().setAttribute(RequestParameterName.ROLE, RequestParameterName.ADMIN);
            User admin = (User) user;
            request.getSession().setAttribute(RequestParameterName.USER, user);
            request.getSession().setAttribute(RequestParameterName.AUTHORIZED_ID, admin.getId());
            return JspPageName.ADMIN_PAGE;
        } else {
            request.setAttribute(RequestParameterName.AUTHORIZATION_ERROR, 1);
            return JspPageName.INDEX_PAGE;
        }
    }
}
