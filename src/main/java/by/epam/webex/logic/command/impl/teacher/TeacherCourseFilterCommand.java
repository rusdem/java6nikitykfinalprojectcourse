package by.epam.webex.logic.command.impl.teacher;


import by.epam.webex.controller.JspPageName;
import by.epam.webex.domain.Course;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.service.DaoAccess;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TeacherCourseFilterCommand implements ICommand {
    private ToTeacherEditCourseCommand toTeacherEditCourseCommand = new ToTeacherEditCourseCommand();

    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        if (request.getSession().getAttribute(RequestParameterName.ROLE) != null) {
            if (request.getSession().getAttribute(RequestParameterName.ROLE).equals(RequestParameterName.TEACHER)) {
                int idCourse;
                if (request.getParameter(RequestParameterName.COURSE_ID) == null) {
                    idCourse = (Integer) request.getSession().getAttribute(RequestParameterName.COURSE_ID);
                } else {
                    idCourse = Integer.parseInt(request.getParameter(RequestParameterName.COURSE_ID));
                }
                request.getSession().setAttribute(RequestParameterName.COURSE_ID, idCourse);
                Course course = DaoAccess.courseDao.select(idCourse);
                request.setAttribute(RequestParameterName.COURSE_NAME, course.getName() + " (" + course.getStartDate() + '/' + course.getEndDate() + ')');
                request.setAttribute(RequestParameterName.TEACHER_COURSES_LIST, DaoAccess.joinScriptDao.selectStudentOnCourseByCourseId(idCourse));
                return toTeacherEditCourseCommand.execute(request, response);
            }
        }
        request.setAttribute(RequestParameterName.ACCESS_ERROR, 1);
        return JspPageName.ERROR_PAGE;
    }
}
