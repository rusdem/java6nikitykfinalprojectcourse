package by.epam.webex.logic.command.impl.teacher;


import by.epam.webex.controller.JspPageName;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.service.DaoAccess;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StudentOnCourseUpdateCommand implements ICommand {
    TeacherCourseFilterCommand teacherCourseFilterCommand = new TeacherCourseFilterCommand();

    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        if (request.getSession().getAttribute(RequestParameterName.ROLE) != null) {
            if (request.getSession().getAttribute(RequestParameterName.ROLE).equals(RequestParameterName.TEACHER)) {
                int studentOnCourse = Integer.parseInt(request.getParameter(RequestParameterName.ID));
                int mark = Integer.parseInt(request.getParameter(RequestParameterName.MARK));
                if(mark>10 || mark<0) {
                    request.setAttribute(RequestParameterName.DATA_ERROR, 1);
                    return JspPageName.ERROR_PAGE;
                }
                String note = request.getParameter(RequestParameterName.NOTE);
                DaoAccess.studentOnCourseDao.update(studentOnCourse,mark,note);
                return teacherCourseFilterCommand.execute(request,response);
            }
        }
        request.setAttribute(RequestParameterName.ACCESS_ERROR, 1);
        return JspPageName.ERROR_PAGE;
    }
}
