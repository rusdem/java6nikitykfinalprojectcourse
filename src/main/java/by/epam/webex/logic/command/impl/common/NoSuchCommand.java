package by.epam.webex.logic.command.impl.common;

import by.epam.webex.controller.JspPageName;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NoSuchCommand implements ICommand {

    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        request.setAttribute(RequestParameterName.COMMAND_ERROR,1);
        return JspPageName.ERROR_PAGE;
    }
}