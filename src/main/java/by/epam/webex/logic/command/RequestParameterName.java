package by.epam.webex.logic.command;

public final class RequestParameterName {
    private RequestParameterName() {
    }
    public static final String COMMAND_NAME = "command";
    public static final String LOCAL_NAME = "local";
    public static final String LOCALIZATION_NAME = "localization";
    public static final String ROLE = "role";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String STUDENT = "student";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String EDUCATION = "education";
    public static final String GRADUATION = "graduation";
    public static final String START_DATE = "start_date";
    public static final String END_DATE = "end_date";
    public static final String LECTOR = "lector";
    public static final String TEACHER = "teacher";
    public static final String ADMIN = "admin";
    public static final String GUEST = "guest";
    public static final String AUTHORIZATION_ERROR = "authorization_error";
    public static final String CURRENT_PAGE = "current_page";
    public static final String DATA_ERROR = "data_error";
    public static final String LOGIN_ERROR = "login_error";
    public static final String COMMAND_ERROR = "command_error";
    public static final String DATABASE_ERROR = "database_error";
    public static final String ACCESS_ERROR = "access_error";
    public static final String AUTHORIZED_ID = "authorized_id";
    public static final String TEACHER_LIST = "teacher_list";
    public static final String COURSE_LIST = "course_list";
    public static final String STUDENT_ON_COURSE_LIST = "student_on_course_list";
    public static final String STUDENT_NOT_ON_COURSE_LIST = "student_not_on_course_list";
    public static final String MIN_DATE = "min_date";
    public static final String MAX_DATE = "max_date";
    public static final String ID = "id";
    public static final String USER = "user";
    public static final String COURSE_ID = "course_id";
    public static final String TEACHER_COURSES_LIST = "teacher_courses_list";
    public static final String OPTIONS = "options";
    public static final String GROUP = "group";
    public static final String COURSE_NAME = "course_name";
    public static final String MARK = "mark";
    public static final String NOTE = "note";
    public static final String NULL_POINTER_ERROR = "null_pointer_error";
} 
