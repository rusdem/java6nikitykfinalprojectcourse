package by.epam.webex.logic.command.impl.common;

import by.epam.webex.controller.JspPageName;
import by.epam.webex.domain.Student;
import by.epam.webex.domain.Teacher;
import by.epam.webex.domain.User;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.command.impl.student.ToStudentPage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ToMainPageCommand implements ICommand {
    private ToStudentPage toStudentPage = new ToStudentPage();

    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        Object user = request.getSession().getAttribute(RequestParameterName.USER);
        if (user != null) {
            if (user.getClass().equals(Student.class)) {
                return toStudentPage.execute(request, response);
            } else if (user.getClass().equals(Teacher.class)) {
                return JspPageName.TEACHER_PAGE;
            } else if (user.getClass().equals(User.class)) {
                return JspPageName.ADMIN_PAGE;
            }
        }
        return JspPageName.INDEX_PAGE;
    }
}