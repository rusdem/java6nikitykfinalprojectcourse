package by.epam.webex.logic.command.impl.student;

import by.epam.webex.controller.JspPageName;
import by.epam.webex.domain.StudentOnCourse;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.service.DaoAccess;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StudentOnCourseAddCommand implements ICommand {
    private ToStudentOnCourseAddCommand toStudentOnCourseAddCommand = new ToStudentOnCourseAddCommand();
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        if (request.getSession().getAttribute(RequestParameterName.ROLE) != null) {
            if (request.getSession().getAttribute(RequestParameterName.ROLE).equals(RequestParameterName.STUDENT)) {
                int userId = Integer.parseInt(request.getParameter(RequestParameterName.ID));
                int courseId = Integer.parseInt(request.getParameter(RequestParameterName.COURSE_ID));
                DaoAccess.studentOnCourseDao.add(new StudentOnCourse(userId, courseId, 0, null));
                return toStudentOnCourseAddCommand.execute(request,response);
            }
        }
        request.setAttribute(RequestParameterName.ACCESS_ERROR, 1);
        return JspPageName.ERROR_PAGE;
    }
}
