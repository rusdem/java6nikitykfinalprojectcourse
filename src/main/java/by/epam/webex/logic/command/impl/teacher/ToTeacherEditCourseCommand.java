package by.epam.webex.logic.command.impl.teacher;

import by.epam.webex.controller.JspPageName;
import by.epam.webex.domain.Teacher;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.service.DaoAccess;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ToTeacherEditCourseCommand implements ICommand {
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        Teacher teacher = null;
        try {
            teacher = (Teacher) request.getSession().getAttribute(RequestParameterName.USER);
        }catch (ClassCastException e)
        {
            request.setAttribute(RequestParameterName.COMMAND_ERROR,"Class Cast Error");
            return JspPageName.ERROR_PAGE;
        }
        if (request.getSession().getAttribute(RequestParameterName.ROLE) != null) {
            if (request.getSession().getAttribute(RequestParameterName.ROLE).equals(RequestParameterName.TEACHER)) {
                int lectorId = teacher.getId();
                request.setAttribute(RequestParameterName.OPTIONS, DaoAccess.courseDao.selectByLector(lectorId));
                return JspPageName.TEACHER_EDIT_COURSE_PAGE;
            }
        }
        request.setAttribute(RequestParameterName.ACCESS_ERROR, 1);
        return JspPageName.ERROR_PAGE;
    }
}
