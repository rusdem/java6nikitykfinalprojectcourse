package by.epam.webex.logic.command.impl.teacher;


import by.epam.webex.controller.JspPageName;
import by.epam.webex.domain.Teacher;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.service.DaoAccess;
import by.epam.webex.logic.service.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TeacherUpdateCommand implements ICommand {
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        Teacher teacher = null;
        int authorizedId = (Integer) request.getSession().getAttribute(RequestParameterName.AUTHORIZED_ID);
        Integer teacherId = Integer.valueOf(request.getParameter(RequestParameterName.ID));
        if (teacherId == authorizedId) {
            String name = request.getParameter(RequestParameterName.NAME);
            String surname = request.getParameter(RequestParameterName.SURNAME);
            String graduation = request.getParameter(RequestParameterName.GRADUATION);
            if (Validator.userValidate(surname, name, graduation)) {
                DaoAccess.teacherDao.update(teacher = new Teacher(authorizedId, surname, name, graduation));
                request.setAttribute(RequestParameterName.TEACHER, teacher);
                request.getSession().setAttribute(RequestParameterName.USER, teacher);
                return JspPageName.TEACHER_PAGE;
            }
            request.setAttribute(RequestParameterName.DATA_ERROR, 1);
            return JspPageName.TEACHER_EDIT_PAGE;
        }
        request.setAttribute(RequestParameterName.ACCESS_ERROR, 1);
        return JspPageName.ERROR_PAGE;
    }
}
