package by.epam.webex.logic.command.impl.student;

import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import by.epam.webex.logic.service.DaoAccess;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StudentOnCourseDeleteCommand implements ICommand {
    private ToStudentPage toStudentPage = new ToStudentPage();
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        if (request.getSession().getAttribute(RequestParameterName.ROLE) != null) {
            if (request.getSession().getAttribute(RequestParameterName.ROLE).equals(RequestParameterName.STUDENT)) {
                int id = Integer.parseInt(request.getParameter(RequestParameterName.ID));
                DaoAccess.studentOnCourseDao.delete(id);
            }
        }
        request.setAttribute(RequestParameterName.ACCESS_ERROR, 1);
        return toStudentPage.execute(request,response);
    }
}
