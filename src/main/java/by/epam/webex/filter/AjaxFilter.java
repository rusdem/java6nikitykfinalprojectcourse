package by.epam.webex.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * The AjaxFilter class sets request and response encoding
 */
//@WebFilter(filterName = "ajax", urlPatterns = "/*")
@WebFilter
public class AjaxFilter implements Filter {
    private static final Logger LOG = Logger.getLogger(AjaxFilter.class);
    private static final String COMMAND = "command";
    private static final String JSON = "json";
    private static final String AJAX_REQUEST = "XMLHttpRequest";
    private static final String REQUEST = "X-Requested-With";

    /**
     * @param filterConfig - a filter configuration object
     * @throws ServletException
     */
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    /**
     * This method is called by the container each time a request/response pair is passed through the chain
     * due to a client request for a resource at the end of the chain. Transforms an AJAX data in request to String data.
     *
     * @param servletRequest  -  an object to provide client request information to a servlet
     * @param servletResponse - object to assist a servlet in sending a response to the client
     * @param filterChain     - an object provided by the servlet container to the developer giving a view into the invocation chain of a filtered request for a resource
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String commandName;
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        if (isAjax(httpRequest)) {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(servletRequest.getInputStream()));
                String json = br.readLine();
                ObjectMapper mapper = new ObjectMapper();
                AjaxRequest ajaxRequest = mapper.readValue(json, AjaxRequest.class);
                commandName = ajaxRequest.getCommand();
                servletRequest.setAttribute(JSON, ajaxRequest.getData());
                servletRequest.setAttribute(COMMAND, commandName);
            } catch (IOException ex) {
                LOG.error(ex);
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    /**
     * Called by the web container to indicate to a filter that it is being taken out of service
     */
    public void destroy() {
    }

    /**
     * Check if request header is an AJAX header
     *
     * @param request - an {@code HttpServletRequest} object that contains the request the client has made of the servlet
     * @return true if header is an AJAX header, false otherwise
     */
    private boolean isAjax(HttpServletRequest request) {
        return AJAX_REQUEST.equals(request.getHeader(REQUEST));
    }

    /**
     * The AjaxRequest class represents data in AJAX format
     */
    public static class AjaxRequest {
        private String command;
        private String data;

        /**
         * Gets command
         *
         * @return command
         */
        public String getCommand() {
            return command;
        }

        /**
         * Sets command
         *
         * @param command specifies command
         */
        public void setCommand(String command) {
            this.command = command;
        }

        /**
         * Gets JSON data
         *
         * @return data in JSON format
         */
        public String getData() {
            return data;
        }

        /**
         * Sets data to request
         *
         * @param data data to set
         */
        public void setData(String data) {
            this.data = data;
        }
    }

}
