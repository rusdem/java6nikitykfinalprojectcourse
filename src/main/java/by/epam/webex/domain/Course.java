package by.epam.webex.domain;

import java.sql.Date;

public class Course {
    private int id;
    private int idLector;
    private String name;
    private Date startDate;
    private Date endDate;

    public Course() {
    }

    public Course(int id) {
        this.id = id;
    }

    public Course(int idLector, String name, Date startDate,Date endDate) {
        this.idLector = idLector;
        this.startDate = startDate;
        this.name = name;
        this.endDate = endDate;
    }

    public Course(int id, int idLector, String name, Date startDate, Date endDate) {
        this.id = id;
        this.idLector = idLector;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdLector() {
        return idLector;
    }

    public void setIdLector(int idLector) {
        this.idLector = idLector;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Course course = (Course) o;

        return id == course.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
