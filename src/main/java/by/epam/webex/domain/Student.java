package by.epam.webex.domain;

public class Student {
    private int id;
    private String name;
    private String surname;
    private String education;

    public Student() {
    }

    public Student(String surname, String name, String education) {
        this.name = name;
        this.surname = surname;
        this.education = education;
    }

    public Student(int id, String surname, String name, String education) {
        this.name = name;
        this.surname = surname;
        this.id = id;
        this.education = education;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        return id == student.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
