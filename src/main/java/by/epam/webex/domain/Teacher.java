package by.epam.webex.domain;

public class Teacher {
    private int id;
    private String name;
    private String surname;
    private String graduation;

    public Teacher() {
    }

    public Teacher(String surname, String name, String graduation) {
        this.name = name;
        this.surname = surname;
        this.graduation = graduation;
    }

    public Teacher(int id, String surname, String name, String graduation) {
        this.name = name;
        this.surname = surname;
        this.id = id;
        this.graduation = graduation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGraduation() {
        return graduation;
    }

    public void setGraduation(String graduation) {
        this.graduation = graduation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Teacher student = (Teacher) o;

        return id == student.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}

