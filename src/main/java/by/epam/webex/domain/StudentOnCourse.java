package by.epam.webex.domain;

public class StudentOnCourse {
    private int id;
    private int studentId;
    private int courseId;
    private int mark;
    private String note;

    public StudentOnCourse() {

    }

    public StudentOnCourse(int studentId, int courseId, int mark, String note) {
        this.studentId = studentId;
        this.courseId = courseId;
        this.mark = mark;
        this.note = note;
    }

    public StudentOnCourse(int id, int studentId, int courseId, int mark, String note) {
        this.id = id;
        this.studentId = studentId;
        this.courseId = courseId;
        this.mark = mark;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StudentOnCourse that = (StudentOnCourse) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
