package by.epam.webex.listener;

import by.epam.webex.dao.ConnectionPool;
import by.epam.webex.logic.resource_manager.PropertyManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
/*
����� ��� ��������� ��������� ���������� � �������� ���� ����������, � ��� �� ��� ����������� ����� ���������� ������.
 */
public class ContextListener implements ServletContextListener {
    private final static Logger log = Logger.getLogger(ContextListener.class);
/*

 */
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        PropertyManager propertyManager = new PropertyManager("project_properties");
        servletContextEvent.getServletContext().setAttribute("local", propertyManager.getValue("localization"));
        if (!ConnectionPool.initConnections(
                propertyManager.getValue("database_driver_class_name"),
                propertyManager.getValue("database_url"),
                propertyManager.getValue("database_username"),
                propertyManager.getValue("database_password"))) {
            log.info("Wrong project properties, connection pool wasn't created");
        }
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ConnectionPool.closeConnections();
    }
}
