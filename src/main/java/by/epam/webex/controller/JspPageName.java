package by.epam.webex.controller;
/*
* ����� �������� ������ ������� � ���� ����������� �����
 */
public final class JspPageName {
    private JspPageName() {
    }
    public static final String INDEX_PAGE = "index.jsp";
    public static final String ERROR_PAGE = "jsp/error.jsp";
    public static final String ADMIN_PAGE = "jsp/adminPage.jsp";
    public static final String STUDENT_PAGE = "jsp/studentPage.jsp";
    public static final String STUDENT_REGISTRATION_PAGE = "jsp/studentRegistration.jsp";
    public static final String STUDENT_EDIT_PAGE = "jsp/studentEditPage.jsp";
    public static final String TEACHER_PAGE = "jsp/teacherPage.jsp";
    public static final String TEACHER_EDIT_PAGE = "jsp/teacherEditPage.jsp";
    public static final String TEACHER_EDIT_COURSE_PAGE = "jsp/teacherEditCoursePage.jsp";
    public static final String COURSE_EDIT_PAGE = "jsp/courseEditPage.jsp";
    public static final String COURSE_ADD_PAGE = "jsp/courseAddPage.jsp";
    public static final String STUDENT_ON_COURSE_ADD_PAGE = "jsp/studentOnCourseAddPage.jsp";
}