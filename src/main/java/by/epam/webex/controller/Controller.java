package by.epam.webex.controller;

import by.epam.webex.dao.DatabaseException;
import by.epam.webex.logic.command.CommandException;
import by.epam.webex.logic.command.CommandHelper;
import by.epam.webex.logic.command.ICommand;
import by.epam.webex.logic.command.RequestParameterName;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class Controller extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private final static Logger log = Logger.getLogger(Controller.class);

    public Controller() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getSession(true).setAttribute(RequestParameterName.LOCAL_NAME, request.getParameter(RequestParameterName.LOCAL_NAME));
        String commandName = null;
        if (isAjax(request)) {
            commandName = (String) request.getAttribute(RequestParameterName.COMMAND_NAME);
        } else {
            commandName = request.getParameter(RequestParameterName.COMMAND_NAME);
        }
        String page = null;
        try {
            ICommand command = CommandHelper.getInstance().getCommand(commandName);
            page = command.execute(request, response);
        } catch (DatabaseException e){
            request.setAttribute(RequestParameterName.DATABASE_ERROR,1);
            log.error(e);
            page = JspPageName.ERROR_PAGE;
        } catch (CommandException e) {
            request.setAttribute(RequestParameterName.COMMAND_ERROR,1);
            log.error(e);
            page = JspPageName.ERROR_PAGE;
        } catch (NullPointerException e) {
            request.setAttribute(RequestParameterName.NULL_POINTER_ERROR,1);
            log.error(e);
            page = JspPageName.ERROR_PAGE;
        } catch (Exception e) {
            log.error(e);
            page = JspPageName.ERROR_PAGE;
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(page);

        if (dispatcher != null) {
            dispatcher.forward(request, response);
        } else {
            errorMessageDireclyFromresponse(response);
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    private void errorMessageDireclyFromresponse(HttpServletResponse response) throws
            IOException {
        response.setContentType("text/html");
        response.getWriter().println("E R R O R");
    }

    private boolean isAjax(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }
} 