package by.epam.webex.dao;

import by.epam.webex.exception.ProjectException;

public class DatabaseException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public DatabaseException(String msg) {
        super(msg);
    }

    public DatabaseException(String msg, Exception e) {
        super(msg, e);
    }

    public DatabaseException(Exception e) {
        super(e);
    }
}