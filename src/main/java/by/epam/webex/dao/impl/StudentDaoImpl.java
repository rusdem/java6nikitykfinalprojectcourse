package by.epam.webex.dao.impl;

import by.epam.webex.dao.ConnectionPool;
import by.epam.webex.dao.Dao;
import by.epam.webex.domain.Student;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class StudentDaoImpl implements Dao<Student> {
    private final static Logger log = Logger.getLogger(StudentDaoImpl.class);

    private final static String ADD_STUDENT = "INSERT INTO student (id, surname, name, education) VALUES (?, ?, ?, ?);";
    private final static String CHECK_EXIST = "SELECT COUNT(*) FROM student WHERE id=?;";
    private final static String DELETE_STUDENT = "DELETE FROM student WHERE id=?;";
    private final static String UPDATE_STUDENT = "UPDATE student SET surname=?, name=?, education=? WHERE id=?;";
    private final static String SELECT_STUDENT = "SELECT id,surname,name,education FROM student WHERE id=?;";
    private final static String SELECT_ALL_STUDENTS = "SELECT id,surname,name,education FROM student;";

    public boolean add(Student ob) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(ADD_STUDENT);
            preparedStatement.setInt(1, ob.getId());
            preparedStatement.setString(2, ob.getSurname());
            preparedStatement.setString(3, ob.getName());
            preparedStatement.setString(4, ob.getEducation());
            boolean result = preparedStatement.execute();
            preparedStatement.close();
            return result;
        } catch (SQLException e) {
            log.info("StudentDao function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return false;
    }

    public int isExist(Student ob) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(CHECK_EXIST);
            preparedStatement.setInt(1, ob.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            int result = -1;
            if (resultSet.next())
                result = resultSet.getInt(1);
            preparedStatement.close();
            return result;
        } catch (SQLException e) {
            log.info("StudentDao function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return -1;
    }

    public int isExist(int id) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(CHECK_EXIST);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            int result = -1;
            if (resultSet.next())
                result = resultSet.getInt(1);
            preparedStatement.close();
            return result;
        } catch (SQLException e) {
            log.info("StudentDao function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return -1;
    }

    public boolean delete(int id) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(DELETE_STUDENT);
            preparedStatement.setInt(1, id);
            return preparedStatement.execute();
        } catch (SQLException e) {
            log.info("StudentDao function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return false;
    }

    public boolean update(Student ob) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(UPDATE_STUDENT);
            preparedStatement.setString(1, ob.getSurname());
            preparedStatement.setString(2, ob.getName());
            preparedStatement.setString(3, ob.getEducation());
            preparedStatement.setInt(4, ob.getId());
            return preparedStatement.execute();
        } catch (SQLException e) {
            log.info("StudentDao function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return false;
    }

    public List<Student> selectAll() {
        LinkedList<Student> list = new LinkedList<Student>();
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SELECT_ALL_STUDENTS);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                list.add(new Student(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4)));
            }
        } catch (SQLException e) {
            log.info("StudentDao function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return list;
    }

    public Student select(int id) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SELECT_STUDENT);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new Student(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4));
            }
        } catch (SQLException e) {
            log.info("StudentDao function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return null;
    }
}