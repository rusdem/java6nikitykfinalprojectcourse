package by.epam.webex.dao.impl;

import by.epam.webex.dao.ConnectionPool;
import by.epam.webex.dao.Dao;
import by.epam.webex.domain.StudentOnCourse;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class StudentOnCourseDaoImpl implements Dao<StudentOnCourse> {
    private final static Logger log = Logger.getLogger(StudentOnCourseDaoImpl.class);

    private final static String ADD_STUDENT_ON_COURSE = "INSERT INTO student_on_course (student_id, course_id, mark, note) VALUES (?, ?, ?, ?);";
    private final static String CHECK_EXIST = "SELECT COUNT(*) FROM student_on_course WHERE id=?;";
    private final static String DELETE_STUDENT_ON_COURSE = "DELETE FROM student_on_course WHERE id=?;";
    private final static String UPDATE_STUDENT_ON_COURSE = "UPDATE student_on_course SET mark=?, note=? WHERE id=?;";
    private final static String SELECT_STUDENT_ON_COURSE = "SELECT id,student_id,course_id,mark,note FROM student_on_course WHERE id=?;";
    private final static String SELECT_ALL_STUDENT_ON_COURSES = "SELECT id,student_id,course_id,mark,note FROM student_on_course;";
    private final static String SELECT_BY_COURSE = "SELECT id,student_id,course_id,mark,note FROM student_on_course WHERE course_id=?;";
    private final static String SELECT_BY_STUDENT = "SELECT id,student_id,course_id,mark,note FROM student_on_course WHERE student_id=?;";
    private final static String SELECT_EXCEPT_STUDENT = "SELECT id,student_id,course_id,mark,note FROM student_on_course WHERE student_id!=?;";


    public boolean add(StudentOnCourse ob) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(ADD_STUDENT_ON_COURSE);
            preparedStatement.setInt(1, ob.getStudentId());
            preparedStatement.setInt(2, ob.getCourseId());
            preparedStatement.setInt(3, ob.getMark());
            preparedStatement.setString(4, ob.getNote());
            boolean result = preparedStatement.execute();
            preparedStatement.close();
            return result;
        } catch (SQLException e) {
            log.info("StudentOnCourseDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentOnCourseDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return false;
    }

    public int isExist(StudentOnCourse ob) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(CHECK_EXIST);
            preparedStatement.setInt(1, ob.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            int result = -1;
            if (resultSet.next())
                result = resultSet.getInt(1);
            preparedStatement.close();
            return result;
        } catch (SQLException e) {
            log.info("StudentOnCourseDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentOnCourseDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return -1;
    }

    public int isExist(int id) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(CHECK_EXIST);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            int result = -1;
            if (resultSet.next())
                result = resultSet.getInt(1);
            preparedStatement.close();
            return result;
        } catch (SQLException e) {
            log.info("StudentOnCourseDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentOnCourseDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return -1;
    }

    public boolean delete(int id) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(DELETE_STUDENT_ON_COURSE);
            preparedStatement.setInt(1, id);
            return preparedStatement.execute();
        } catch (SQLException e) {
            log.info("StudentOnCourseDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentOnCourseDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return false;
    }

    public boolean update(StudentOnCourse ob) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(UPDATE_STUDENT_ON_COURSE);
            preparedStatement.setInt(1, ob.getMark());
            preparedStatement.setString(2, ob.getNote());
            preparedStatement.setInt(3, ob.getId());
            return preparedStatement.execute();
        } catch (SQLException e) {
            log.info("StudentOnCourseDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentOnCourseDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return false;
    }

    public boolean update(int id, int mark, String note) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(UPDATE_STUDENT_ON_COURSE);
            preparedStatement.setInt(1, mark);
            preparedStatement.setString(2, note);
            preparedStatement.setInt(3, id);
            return preparedStatement.execute();
        } catch (SQLException e) {
            log.info("StudentOnCourseDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentOnCourseDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return false;
    }

    public List<StudentOnCourse> selectAll() {
        LinkedList<StudentOnCourse> list = new LinkedList<StudentOnCourse>();
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SELECT_ALL_STUDENT_ON_COURSES);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                list.add(new StudentOnCourse(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getInt(4), resultSet.getString(5)));
            }
        } catch (SQLException e) {
            log.info("StudentOnCourseDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentOnCourseDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return list;
    }


    public StudentOnCourse select(int id) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SELECT_STUDENT_ON_COURSE);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new StudentOnCourse(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getInt(4), resultSet.getString(5));
            }
        } catch (SQLException e) {
            log.info("StudentOnCourseDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentOnCourseDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return null;
    }

    public List<StudentOnCourse> selectByLector(int id) {
        LinkedList<StudentOnCourse> list = new LinkedList<StudentOnCourse>();
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SELECT_BY_COURSE);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                list.add(new StudentOnCourse(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getInt(4), resultSet.getString(5)));
            }
        } catch (SQLException e) {
            log.info("StudentOnCourseDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentOnCourseDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return list;
    }

    public List<StudentOnCourse> selectByStudent(Integer id) {
        LinkedList<StudentOnCourse> list = new LinkedList<StudentOnCourse>();
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SELECT_BY_STUDENT);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                list.add(new StudentOnCourse(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getInt(4), resultSet.getString(5)));
            }
        } catch (SQLException e) {
            log.info("StudentOnCourseDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentOnCourseDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return list;
    }

    public List<StudentOnCourse> selectExceptStudent(Integer id) {
        LinkedList<StudentOnCourse> list = new LinkedList<StudentOnCourse>();
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SELECT_EXCEPT_STUDENT);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                list.add(new StudentOnCourse(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getInt(4), resultSet.getString(5)));
            }
        } catch (SQLException e) {
            log.info("StudentOnCourseDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("StudentOnCourseDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return list;
    }

}