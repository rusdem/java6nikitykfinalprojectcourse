package by.epam.webex.dao.impl;

import by.epam.webex.dao.ConnectionPool;
import by.epam.webex.dao.Dao;
import by.epam.webex.domain.Course;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class CourseDaoImpl implements Dao<Course> {
    private final static Logger log = Logger.getLogger(CourseDaoImpl.class);

    private final static String ADD_COURSE = "INSERT INTO course (lector, name, date_start, date_end) VALUES (?, ?, ?, ?);";
    private final static String CHECK_EXIST = "SELECT COUNT(*) FROM course WHERE id_course=?;";
    private final static String DELETE_COURSE = "DELETE FROM course WHERE id_course=?;";
    private final static String UPDATE_COURSE = "UPDATE course SET lector=?, name=?, date_start=?, date_end=? WHERE id_course=?;";
    private final static String SELECT_COURSE = "SELECT id_course,lector,name,date_start,date_end FROM course WHERE id_course=?;";
    private final static String SELECT_ALL_COURSES = "SELECT id_course,lector,name,date_start,date_end FROM course;";
    private final static String SELECT_BY_LECTOR = "SELECT id_course,lector,name,date_start,date_end FROM course WHERE lector=?;";
    private final static String GET_LECTOR_ID_BY_COURSE = "SELECT lector FROM course WHERE id_course=?;";
    private final static String SELECT_BY_DATE = "SELECT id_course,lector,name,date_start,date_end FROM course WHERE date_start>=? AND date_end<=?;";


    public boolean add(Course ob) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(ADD_COURSE);
            preparedStatement.setInt(1, ob.getIdLector());
            preparedStatement.setString(2, ob.getName());
            preparedStatement.setDate(3, ob.getStartDate());
            preparedStatement.setDate(4, ob.getEndDate());
            boolean result = preparedStatement.execute();
            return result;
        } catch (SQLException e) {
            log.info("CourseDaoImpl function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("CourseDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return false;
    }

    public int isExist(Course ob) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(CHECK_EXIST);
            preparedStatement.setInt(1, ob.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            int result = -1;
            if (resultSet.next())
                result = resultSet.getInt(1);
            return result;
        } catch (SQLException e) {
            log.info("CourseDaoImpl function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("CourseDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return -1;
    }

    public int isExist(int id) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(CHECK_EXIST);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            int result = -1;
            if (resultSet.next())
                result = resultSet.getInt(1);
            return result;
        } catch (SQLException e) {
            log.info("CourseDaoImpl function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("CourseDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return -1;
    }

    public boolean delete(int id) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(DELETE_COURSE);
            preparedStatement.setInt(1, id);
            return preparedStatement.execute();
        } catch (SQLException e) {
            log.info("CourseDaoImpl function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("CourseDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return false;
    }

    public boolean update(Course ob) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(UPDATE_COURSE);
            preparedStatement.setInt(1, ob.getIdLector());
            preparedStatement.setString(2, ob.getName());
            preparedStatement.setDate(3, ob.getStartDate());
            preparedStatement.setDate(4, ob.getEndDate());
            preparedStatement.setInt(5, ob.getId());
            return preparedStatement.execute();
        } catch (SQLException e) {
            log.info("CourseDaoImpl function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("CourseDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return false;
    }

    public List<Course> selectAll() {
        LinkedList<Course> list = new LinkedList<Course>();
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SELECT_ALL_COURSES);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                list.add(new Course(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getDate(4), resultSet.getDate(5)));
            }
        } catch (SQLException e) {
            log.info("CourseDaoImpl function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("CourseDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return list;
    }

    public List<Course> selectByDate(Date start, Date end) {
        LinkedList<Course> list = new LinkedList<Course>();
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SELECT_BY_DATE);
            preparedStatement.setDate(1, start);
            preparedStatement.setDate(2, end);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                list.add(new Course(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getDate(4), resultSet.getDate(5)));
            }
        } catch (SQLException e) {
            log.info("CourseDaoImpl function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("CourseDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return list;
    }

    public Course select(int id) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SELECT_COURSE);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new Course(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getDate(4), resultSet.getDate(5));
            }
        } catch (SQLException e) {
            log.info("CourseDaoImpl function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("CourseDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return null;
    }

    public int getLectorId(int id) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_LECTOR_ID_BY_COURSE);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            log.info("CourseDao function error", e);
            return -1;
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("CourseDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return -1;
    }

    public List<Course> selectByLector(int id) {
        LinkedList<Course> list = new LinkedList<Course>();
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SELECT_BY_LECTOR);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                list.add(new Course(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getDate(4), resultSet.getDate(5)));
            }
        } catch (SQLException e) {
            log.info("CourseDaoImpl function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("CourseDao preparedStatement.close() error ", e);
            }
            connection.close();
        }
        return list;
    }
}