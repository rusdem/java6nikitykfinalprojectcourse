package by.epam.webex.dao.impl;

import by.epam.webex.dao.ConnectionPool;
import by.epam.webex.dao.Dao;
import by.epam.webex.domain.Teacher;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class TeacherDaoImpl implements Dao<Teacher> {
    private final static Logger log = Logger.getLogger(TeacherDaoImpl.class);

    private final static String ADD_TEACHER = "INSERT INTO lector (id, surname, name, graduation) VALUES (?, ?, ?, ?);";
    private final static String CHECK_EXIST = "SELECT COUNT(*) FROM lector WHERE id=?;";
    private final static String DELETE_TEACHER = "DELETE FROM lector WHERE id=?;";
    private final static String UPDATE_TEACHER = "UPDATE lector SET surname=?, name=?, graduation=? WHERE id=?;";
    private final static String SELECT_TEACHER = "SELECT id,surname,name,graduation FROM lector WHERE id=?;";
    private final static String SELECT_ALL_TEACHERS = "SELECT id,surname,name,graduation FROM lector;";

    public boolean add(Teacher ob) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(ADD_TEACHER);
            preparedStatement.setInt(1, ob.getId());
            preparedStatement.setString(2, ob.getSurname());
            preparedStatement.setString(3, ob.getName());
            preparedStatement.setString(4, ob.getGraduation());
            boolean result = preparedStatement.execute();
            preparedStatement.close();
            return result;
        } catch (SQLException e) {
            log.info("TeacherDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("TeacherDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return false;
    }

    public int isExist(Teacher ob) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(CHECK_EXIST);
            preparedStatement.setInt(1, ob.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            int result = -1;
            if (resultSet.next())
                result = resultSet.getInt(1);
            preparedStatement.close();
            return result;
        } catch (SQLException e) {
            log.info("TeacherDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("TeacherDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return -1;
    }

    public int isExist(int id) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(CHECK_EXIST);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            int result = -1;
            if (resultSet.next())
                result = resultSet.getInt(1);
            preparedStatement.close();
            return result;
        } catch (SQLException e) {
            log.info("TeacherDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("TeacherDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return -1;
    }

    public boolean delete(int id) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(DELETE_TEACHER);
            preparedStatement.setInt(1, id);
            return preparedStatement.execute();
        } catch (SQLException e) {
            log.info("TeacherDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("TeacherDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return false;
    }

    public boolean update(Teacher ob) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(UPDATE_TEACHER);
            preparedStatement.setString(1, ob.getSurname());
            preparedStatement.setString(2, ob.getName());
            preparedStatement.setString(3, ob.getGraduation());
            preparedStatement.setInt(4, ob.getId());
            return preparedStatement.execute();
        } catch (SQLException e) {
            log.info("TeacherDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("TeacherDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return false;
    }

    public List<Teacher> selectAll() {
        LinkedList<Teacher> list = new LinkedList<Teacher>();
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SELECT_ALL_TEACHERS);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                list.add(new Teacher(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4)));
            }
        } catch (SQLException e) {
            log.info("TeacherDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("TeacherDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return list;
    }

    public Teacher select(int id) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SELECT_TEACHER);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new Teacher(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4));
            }
        } catch (SQLException e) {
            log.info("TeacherDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("TeacherDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return null;
    }
}