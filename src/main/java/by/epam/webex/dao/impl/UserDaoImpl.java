package by.epam.webex.dao.impl;

import by.epam.webex.dao.ConnectionPool;
import by.epam.webex.dao.Dao;
import by.epam.webex.domain.User;
import by.epam.webex.logic.service.Encryptor;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class UserDaoImpl implements Dao<User> {
    private final static Logger log = Logger.getLogger(UserDaoImpl.class);

    private final static String ADD_USER = "INSERT INTO logpass (login, password, role) VALUES (?, ?, ?);";
    private final static String CHECK_USER_ROLE = "SELECT role FROM logpass WHERE login=? AND password=?;";
    private final static String CHECK_USER_EXIST = "SELECT COUNT(*) FROM logpass WHERE login=?;";
    private final static String DELETE_USER = "DELETE FROM logpass WHERE id_logpass=?;";
    private final static String ID_USER = "SELECT id_logpass FROM logpass WHERE login=?;";
    private final static String UPDATE_USER = "UPDATE logpass SET login=?, password=?, role=? WHERE id_logpass=?;";
    private final static String SELECT_USER = "SELECT id_logpass,login,password,role FROM logpass WHERE id_logpass=?;";
    private final static String SELECT_ALL_USERS = "SELECT id_logpass,login,password,role FROM logpass;";

    public boolean add(User ob) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(ADD_USER);
            preparedStatement.setString(1, ob.getLogin());
            preparedStatement.setString(2, Encryptor.hashEncryption(ob.getPassword()));
            preparedStatement.setString(3, ob.getRole());
            boolean result = preparedStatement.execute();
            preparedStatement.close();
            return result;
        } catch (SQLException e) {
            log.info("UserDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("UserDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return false;
    }

    public int getId(String login) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(ID_USER);
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            int result = -1;
            if (resultSet.next())
                result = resultSet.getInt(1);
            preparedStatement.close();
            return result;
        } catch (SQLException e) {
            log.info("UserDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("UserDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return -1;
    }

    public int isExist(User ob) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(CHECK_USER_EXIST);
            preparedStatement.setString(1, ob.getLogin());
            ResultSet resultSet = preparedStatement.executeQuery();
            int result = -1;
            if (resultSet.next())
                result = resultSet.getInt(1);
            preparedStatement.close();
            return result;
        } catch (SQLException e) {
            log.info("UserDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("UserDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return -1;
    }

    public int isExist(String login) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(CHECK_USER_EXIST);
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            int result = -1;
            if (resultSet.next())
                result = resultSet.getInt(1);
            preparedStatement.close();
            return result;
        } catch (SQLException e) {
            log.info("UserDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("UserDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return -1;
    }

    public String checkRole(User ob) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(CHECK_USER_ROLE);
            preparedStatement.setString(1, ob.getLogin());
            preparedStatement.setString(2, ob.getPassword());
            ResultSet resultSet = preparedStatement.executeQuery();
            String result = null;
            if (resultSet.next())
                result = resultSet.getString(1);
            preparedStatement.close();
            return result;
        } catch (SQLException e) {
            log.info("UserDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("UserDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return null;
    }

    public boolean delete(int id) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(DELETE_USER);
            preparedStatement.setInt(1, id);
            return preparedStatement.execute();
        } catch (SQLException e) {
            log.info("UserDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("UserDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return false;
    }

    public boolean update(User ob) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(UPDATE_USER);
            preparedStatement.setString(1, ob.getLogin());
            preparedStatement.setString(2, Encryptor.hashEncryption(ob.getPassword()));
            preparedStatement.setString(3, ob.getRole());
            preparedStatement.setInt(4, ob.getId());
            return preparedStatement.execute();
        } catch (SQLException e) {
            log.info("UserDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("UserDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return false;
    }

    public List<User> selectAll() {
        LinkedList<User> list = new LinkedList<User>();
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SELECT_ALL_USERS);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                list.add(new User(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4)));
            }
        } catch (SQLException e) {
            log.info("UserDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("UserDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return list;
    }

    public User select(int id) {
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SELECT_USER);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new User(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4));
            }
        } catch (SQLException e) {
            log.info("UserDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("UserDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return null;
    }
}
