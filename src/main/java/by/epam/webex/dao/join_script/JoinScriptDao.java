package by.epam.webex.dao.join_script;

import by.epam.webex.dao.ConnectionPool;
import by.epam.webex.domain.Course;
import by.epam.webex.domain.StudentOnCourse;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
/*
����� ������� � ���� ������ � ����������� ������ � ����� ��� ����� ���������.
 */
public class JoinScriptDao {
    private final static Logger log = Logger.getLogger(JoinScriptDao.class);

    private final static String COURSE_WITH_STUDENT = "SELECT student_on_course.id, student_on_course.student_id, student_on_course.course_id, student_on_course.mark, student_on_course.note, course.id_course, course.name, course.lector, course.date_start, course.date_end, lector.surname, lector.name FROM (student_on_course INNER JOIN course ON student_on_course.course_id = course.id_course) INNER JOIN lector ON course.lector = lector.id WHERE student_on_course.student_id=?;";
    private final static String COURSE_EXCEPT_STUDENT = "SELECT course.id_course, course.lector, course.name, course.date_start, course.date_end, lector.surname, lector.name FROM (lector INNER JOIN course ON course.lector = lector.id) LEFT OUTER JOIN student_on_course on student_on_course.course_id=course.id_course WHERE course.date_start>=? and student_on_course.student_id IS NULL OR student_on_course.student_id !=?;";
    private final static String COURSE_BY_TEACHER = "SELECT student_on_course.id, student_on_course.student_id, student_on_course.course_id,student_on_course.mark, student_on_course.note, student.surname, student.name, student.education FROM student_on_course inner join student on student.id=student_on_course.student_id where student_on_course.course_id=?;";

    public List<Object[]> selectStudentCourseByStudent(int id) {
        LinkedList<Object[]> list = new LinkedList<Object[]>();
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(COURSE_WITH_STUDENT);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            StudentOnCourse studentOnCourse = null;
            String teacherSurnameName = null;
            Course course = null;
            Object[] outputObjects = null;
            while (resultSet.next()) {
                studentOnCourse = new StudentOnCourse(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getInt(4), resultSet.getString(5));
                course = new Course(resultSet.getInt(6), resultSet.getInt(8), resultSet.getString(7), resultSet.getDate(9), resultSet.getDate(10));
                teacherSurnameName = resultSet.getString(11) + " " + resultSet.getString(12);
                outputObjects = new Object[3];
                outputObjects[0] = studentOnCourse;
                outputObjects[1] = course;
                outputObjects[2] = teacherSurnameName;
                list.add(outputObjects);
            }
        } catch (SQLException e) {
            log.info("JoinScriptDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("JoinScriptDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return list;
    }

    public List<Object[]> selectStudentCourseExceptStudentId(int id) {
        java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
        LinkedList<Object[]> list = new LinkedList<Object[]>();
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(COURSE_EXCEPT_STUDENT);
            preparedStatement.setDate(1, sqlDate);
            preparedStatement.setInt(2, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            StudentOnCourse studentOnCourse = null;
            String teacherSurnameName = null;
            Course course = null;
            Object[] outputObjects = null;
            while (resultSet.next()) {
                course = new Course(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getDate(4), resultSet.getDate(5));
                teacherSurnameName = resultSet.getString(6) + " " + resultSet.getString(7);
                outputObjects = new Object[2];
                outputObjects[0] = course;
                outputObjects[1] = teacherSurnameName;
                list.add(outputObjects);
            }
        } catch (SQLException e) {
            log.info("JoinScriptDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("JoinScriptDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return list;
    }

    public List<Object[]> selectStudentOnCourseByCourseId(int id) {
        LinkedList<Object[]> list = new LinkedList<Object[]>();
        ConnectionPool.PooledConnection connection = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(COURSE_BY_TEACHER);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            StudentOnCourse studentOnCourse = null;
            String studentSurnameName = null;
            Object[] outputObjects = null;
            while (resultSet.next()) {
                studentOnCourse = new StudentOnCourse(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getInt(4), resultSet.getString(5));
                studentSurnameName = resultSet.getString(6) + " " + resultSet.getString(7);
                outputObjects = new Object[3];
                outputObjects[0] = studentOnCourse;
                outputObjects[1] = studentSurnameName;
                outputObjects[2] = resultSet.getString(8);
                list.add(outputObjects);
            }
        } catch (SQLException e) {
            log.info("JoinScriptDao: function error", e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                log.info("JoinScriptDao: preparedStatement.close() error", e);
            }
            connection.close();
        }
        return list;
    }
}
