package by.epam.webex.dao;

import java.util.List;
/*
Шаблонный интерфейс для доступа к базе данных.
 */
public interface Dao<T> {
    boolean add(T ob);

    boolean delete(int id);

    boolean update(T ob);

    List<T> selectAll();

    T select(int id);
}
