package by.epam.webex.dao;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executor;
/*
Пул соединений - класс для ограничения количества открытых соединений, имеет 2 потокобезопасные очереди
для хранения свободных и используемых в данный момент соединений, при окончании использования для используемого
соединения вызывается переопределённый во вложенном классе PooledConnection метод close(), который
определяет необходимость закрытия или возвращения соединения в пул. Булевая переменная readyForWorkFlag
показывает готовность к работе или необходимость закрытия соединений.
 */
public class ConnectionPool {
    private final static Logger log = Logger.getLogger(ConnectionPool.class);
    private static boolean readyForWorkFlag = false;
    private static int maxConnections = 10;
    private static ArrayBlockingQueue<PooledConnection> freeConnections = new ArrayBlockingQueue<PooledConnection>(maxConnections);
    private static ArrayBlockingQueue<PooledConnection> bizyConnections = new ArrayBlockingQueue<PooledConnection>(maxConnections);


    public static class PooledConnection implements Connection {
        private Connection connection;

        public PooledConnection(Connection c) throws SQLException {
            this.connection = c;
            this.connection.setAutoCommit(true);
        }
/*
    Метод-надстройка над стандартным, если флаг работы readyForWorkFlag равен true, то соединение
    возвращается в пул, если флаг равен false, то соединение закрывается
 */

        public void close() {
            if (readyForWorkFlag) {
                bizyConnections.remove(this);
                freeConnections.add(this);
            } else {
                try {
                    connection.close();
                } catch (SQLException e) {
                    log.info("error in close connection", e);
                }
            }
        }

        public void clearWarnings() throws SQLException {
            connection.clearWarnings();
        }

        public void commit() throws SQLException {
            connection.commit();
        }

        public Array createArrayOf(String typeName, Object[] elements)
                throws SQLException {
            return connection.createArrayOf(typeName, elements);
        }

        public Blob createBlob() throws SQLException {
            return connection.createBlob();
        }

        public Clob createClob() throws SQLException {
            return connection.createClob();
        }

        public NClob createNClob() throws SQLException {
            return connection.createNClob();
        }

        public SQLXML createSQLXML() throws SQLException {
            return connection.createSQLXML();
        }

        public Statement createStatement() throws SQLException {
            return connection.createStatement();
        }

        public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.createStatement(resultSetType,
                    resultSetConcurrency);
        }

        public Statement createStatement(int resultSetType,
                                         int resultSetConcurrency, int resultSetHoldability)
                throws SQLException {
            return connection.createStatement(resultSetType,
                    resultSetConcurrency, resultSetHoldability);
        }

        public Struct createStruct(String typeName, Object[] attributes)
                throws SQLException {
            return connection.createStruct(typeName, attributes);
        }

        public boolean getAutoCommit() throws SQLException {
            return connection.getAutoCommit();
        }

        public String getCatalog() throws SQLException {
            return connection.getCatalog();
        }

        public Properties getClientInfo() throws SQLException {
            return connection.getClientInfo();
        }

        public String getClientInfo(String name) throws SQLException {
            return connection.getClientInfo(name);
        }

        public int getHoldability() throws SQLException {
            return connection.getHoldability();
        }

        public DatabaseMetaData getMetaData() throws SQLException {
            return connection.getMetaData();
        }

        public int getTransactionIsolation() throws SQLException {
            return connection.getTransactionIsolation();
        }

        public Map<String, Class<?>> getTypeMap() throws SQLException {
            return connection.getTypeMap();
        }

        public SQLWarning getWarnings() throws SQLException {
            return connection.getWarnings();
        }

        public boolean isClosed() throws SQLException {
            return connection.isClosed();
        }

        public boolean isReadOnly() throws SQLException {
            return connection.isReadOnly();
        }

        public boolean isValid(int timeout) throws SQLException {
            return connection.isValid(timeout);
        }

        public String nativeSQL(String sql) throws SQLException {
            return connection.nativeSQL(sql);
        }

        public CallableStatement prepareCall(String sql) throws SQLException {
            return connection.prepareCall(sql);
        }

        public CallableStatement prepareCall(String sql, int resultSetType,
                                             int resultSetConcurrency) throws SQLException {
            return connection.prepareCall(sql, resultSetType,
                    resultSetConcurrency);
        }

        public CallableStatement prepareCall(String sql, int resultSetType,
                                             int resultSetConcurrency, int resultSetHoldability)
                throws SQLException {
            return connection.prepareCall(sql, resultSetType,
                    resultSetConcurrency, resultSetHoldability);
        }

        public PreparedStatement prepareStatement(String sql)
                throws SQLException {
            return connection.prepareStatement(sql);
        }

        public PreparedStatement prepareStatement(String sql,
                                                  int autoGeneratedKeys) throws SQLException {
            return connection.prepareStatement(sql, autoGeneratedKeys);
        }

        public PreparedStatement prepareStatement(String sql,
                                                  int[] columnIndexes) throws SQLException {
            return connection.prepareStatement(sql, columnIndexes);
        }

        public PreparedStatement prepareStatement(String sql,
                                                  String[] columnNames) throws SQLException {
            return connection.prepareStatement(sql, columnNames);
        }

        public PreparedStatement prepareStatement(String sql,
                                                  int resultSetType, int resultSetConcurrency)
                throws SQLException {
            return connection.prepareStatement(sql, resultSetType,
                    resultSetConcurrency);
        }

        public PreparedStatement prepareStatement(String sql,
                                                  int resultSetType, int resultSetConcurrency,
                                                  int resultSetHoldability) throws SQLException {
            return connection.prepareStatement(sql, resultSetType,
                    resultSetConcurrency, resultSetHoldability);
        }

        public void rollback() throws SQLException {
            connection.rollback();
        }

        public void setAutoCommit(boolean autoCommit) throws SQLException {
            connection.setAutoCommit(autoCommit);
        }

        public void setCatalog(String catalog) throws SQLException {
            connection.setCatalog(catalog);
        }

        public void setClientInfo(String name, String value)
                throws SQLClientInfoException {
            connection.setClientInfo(name, value);
        }

        public void setHoldability(int holdability) throws SQLException {
            connection.setHoldability(holdability);
        }

        public void setReadOnly(boolean readOnly) throws SQLException {
            connection.setReadOnly(readOnly);
        }

        public Savepoint setSavepoint() throws SQLException {
            return connection.setSavepoint();
        }

        public Savepoint setSavepoint(String name) throws SQLException {
            return connection.setSavepoint(name);
        }

        public void setTransactionIsolation(int level) throws SQLException {
            connection.setTransactionIsolation(level);
        }

        public boolean isWrapperFor(Class<?> iface) throws SQLException {
            return connection.isWrapperFor(iface);
        }

        public <T> T unwrap(Class<T> iface) throws SQLException {
            return connection.unwrap(iface);
        }

        public void abort(Executor arg0) throws SQLException {
            connection.abort(arg0);

        }

        public int getNetworkTimeout() throws SQLException {
            return connection.getNetworkTimeout();
        }

        public String getSchema() throws SQLException {
            return connection.getSchema();
        }

        public void releaseSavepoint(Savepoint arg0) throws SQLException {
            connection.releaseSavepoint(arg0);
        }

        public void rollback(Savepoint arg0) throws SQLException {
            connection.rollback(arg0);
        }

        public void setClientInfo(Properties arg0)
                throws SQLClientInfoException {
            connection.setClientInfo(arg0);
        }

        public void setNetworkTimeout(Executor arg0, int arg1)
                throws SQLException {
            connection.setNetworkTimeout(arg0, arg1);
        }

        public void setSchema(String arg0) throws SQLException {
            connection.setSchema(arg0);
        }

        public void setTypeMap(Map<String, Class<?>> arg0) throws SQLException {
            connection.setTypeMap(arg0);
        }
    }
/*
* Метод инициализыции пула соединений
* @returns true - при установке соединений, false - при неудаче
 */
    public static boolean initConnections(String databaseDriverClassName, String databaseUrl, String databaseUsername, String databasePassword) {
        BasicConfigurator.configure();
        readyForWorkFlag = true;
        try {
            Class.forName(databaseDriverClassName);
        } catch (ClassNotFoundException e) {
            log.info("database driver error", e);
            readyForWorkFlag = false;
        }
        try {
            for (int i = 0; i != maxConnections; i++) {
                freeConnections.add(new PooledConnection(DriverManager.getConnection(databaseUrl, databaseUsername, databasePassword)));
            }
            log.info("initialization is finished");
        } catch (SQLException e) {
            log.info("database connection error", e);
            readyForWorkFlag = false;
        }
        return readyForWorkFlag;
    }

    public static PooledConnection getConnection() throws DatabaseException {
        if (readyForWorkFlag) {
            try {
                log.info("getting connect");
                PooledConnection connection = freeConnections.take();
                bizyConnections.add(connection);
                return connection;
            } catch (InterruptedException e) {
                log.info("error ", e);
            }
        }
        throw new DatabaseException("Database is offline");
    }
/*
    Закрытие соединений. Установка флага отказа в выдаче соединений и закрытие свободных соединений,
    для занятых соединений выдержка времени для завершения работ ними и их последующее закрытие.
    @returns true если соединения закрыты, false при наличии неполадок
 */
    public static boolean closeConnections() {
        readyForWorkFlag = false;
        boolean closeFlag = true;
        try {
            for (Connection con : freeConnections) {
                con.close();
            }
            log.info("Free connections are closed");
            Thread.sleep(2000);
            for (Connection con : bizyConnections) {
                con.close();
            }
            log.info("Busy connections are closed");
        } catch (InterruptedException e) {
            log.info("close error", e);
        } catch (SQLException e) {
            closeFlag = false;
            log.info("close error", e);
        }
        return closeFlag;
    }
}

