<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@include file="header.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Welcome, User</title>
    <link rel='stylesheet' href='css_js/css/bootstrap.min.css' type='text/css' media='all'>
    <script src="css_js/js/jquery-1.3.2.min.js"></script>
    <script src='css_js/js/myfunc.js'></script>
    <fmt:setLocale value="${sessionScope.localization}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="back" var="back"/>
    <fmt:message bundle="${loc}" key="course_time_ammount" var="course_time_ammount"/>
    <fmt:message bundle="${loc}" key="search" var="search"/>
    <fmt:message bundle="${loc}" key="min_border" var="min_border"/>
    <fmt:message bundle="${loc}" key="max_border" var="max_border"/>
    <fmt:message bundle="${loc}" key="course_name_label" var="course_name"/>
    <fmt:message bundle="${loc}" key="course_lector_label" var="course_lector"/>
    <fmt:message bundle="${loc}" key="course_start_date" var="course_start_date"/>
    <fmt:message bundle="${loc}" key="course_end_date" var="course_end_date"/>
    <fmt:message bundle="${loc}" key="save" var="save"/>
    <fmt:message bundle="${loc}" key="delete" var="delete"/>
    <fmt:message bundle="${loc}" key="course_edit_label" var="edit_label"/>
    <fmt:message bundle="${loc}" key="textPattern.regexp" var="textPattern"/>
    <fmt:message bundle="${loc}" key="data_error" var="data_error"/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <br/>

        <div class="col-xs-10 col-sm-offset-1" style="background-color: white;opacity: 0.9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title" style="text-align: center">${course_time_ammount}</h3>
                </div>
                <div class="panel-body">
                    <form action="controller" class="form-horizontal" method="get">
                        <input type="hidden" name="command" value="course_filter_command">

                        <div class="form-group">
                            <label for="inputMinDate" class="col-sm-2 control-label">${min_border}</label>

                            <div class="col-sm-10">
                                <input type="date" name="min_date" class="form-control"
                                       value="${min_date}"
                                       onclick="startDateInit('inputMinDate','inputMaxDate','01-01-2000')"
                                       onchange="endDateInit('inputMinDate','inputMaxDate')"
                                       id="inputMinDate">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputMaxDate" class="col-sm-2 control-label">${max_border}</label>

                            <div class="col-sm-10">
                                <input type="date" name="max_date" class="form-control"
                                       value="${max_date}"
                                       id="inputMaxDate">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="submit" style="width: 100%"
                                       class="btn btn-primary" value="${search}"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title" style="text-align: center">${edit_label}</h3>
                </div>
                <div class="panel-body">
                    <c:if test="${requestScope.data_error!=null}">
                        <div class="alert alert-warning alert-dismissable">
                            <strong>${data_error}</strong>
                        </div>
                    </c:if>
                    <table class="table table-hover">
                        <tr>
                            <td>
                                <h4>${course_name}</h4>
                            </td>
                            <td>
                                <h4>${course_lector}</h4>
                            </td>
                            <td>
                                <h4>${course_start_date}</h4>
                            </td>
                            <td>
                                <h4>${course_end_date}</h4>
                            </td>
                            <td>
                                <h4>${save}</h4>
                            </td>
                            <td>
                                <h4>${delete}</h4>
                            </td>
                        </tr>
                        <c:forEach var="course" items="${course_list}">
                            <tr>
                                <form id="update_form" action="controller" class="form-horizontal" method="post">
                                    <input type="hidden" name="command" value="course_update_command">
                                    <input type="hidden" name="id" value="${course.id}">
                                    <td>
                                        <input type="text" value="${course.name}" pattern="${textPattern}" name="name" class="form-control">
                                    </td>
                                    <td>
                                        <select id="lector" name="lector" class="form-control">
                                            <c:forEach var="teacher" items="${requestScope.teacher_list}">
                                                <c:if test="${teacher.id==course.idLector}">
                                                    <option selected="selected"
                                                            value="${teacher.id}">${teacher.surname} ${teacher.name}</option>
                                                </c:if>
                                                <c:if test="${teacher.id!=course.idLector}">
                                                    <option value="${teacher.id}">${teacher.surname} ${teacher.name}</option>
                                                </c:if>
                                            </c:forEach>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="date" name="start_date" class="form-control"
                                               onclick="startDateInit('inputStartDate','inputEndDate',null)"
                                               onchange="endDateInit('inputStartDate','inputEndDate')"
                                               id="inputStartDate" value="${course.startDate}" required>
                                    <td>
                                        <input type="date" name="end_date" class="form-control"
                                               id="inputEndDate" value="${course.endDate}" required>
                                    </td>
                                    <td>
                                        <input type="submit" style="width: 100%" class="btn btn-primary"
                                               value="${save}"/>
                                    </td>
                                </form>
                                <td>
                                    <input type="submit" style="width: 100%" class="btn btn-danger"
                                           onclick="deleteCourse(${course.id},this)"
                                           value="${delete}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-offset-0 col-sm-12">
                        <form id="to_main_form" action="controller" method="post">
                            <input type="hidden" name="command" value="to_main_page_command">
                            <button type="submit" style="width: 100%" class="btn btn-primary">${back}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
