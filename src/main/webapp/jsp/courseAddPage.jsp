<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@include file="header.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Welcome, Admin</title>
    <link rel='stylesheet' href='css_js/css/bootstrap.min.css' type='text/css' media='all'>
    <script src='css_js/js/myfunc.js'></script>
    <fmt:setLocale value="${sessionScope.localization}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="back" var="back"/>
    <fmt:message bundle="${loc}" key="course_add_label" var="course_add_label"/>
    <fmt:message bundle="${loc}" key="course_name_label" var="course_name"/>
    <fmt:message bundle="${loc}" key="course_lector_label" var="course_lector"/>
    <fmt:message bundle="${loc}" key="course_start_date" var="course_start_date"/>
    <fmt:message bundle="${loc}" key="course_end_date" var="course_end_date"/>
    <fmt:message bundle="${loc}" key="textPattern.regexp" var="textPattern"/>
    <fmt:message bundle="${loc}" key="data_error" var="data_error"/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <br/>

        <div class="col-xs-10 col-sm-offset-1" style="background-color: white;opacity: 0.9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title" style="text-align: center">${course_add_label}</h3>
                </div>
                <div class="panel-body">
                    <c:if test="${requestScope.data_error!=null}">
                        <div class="alert alert-warning alert-dismissable">
                            <strong>${data_error}</strong>
                        </div>
                    </c:if>
                    <form action="controller" class="form-horizontal" method="post">
                        <input type="hidden" name="command" value="course_add_command">

                        <div class="form-group">
                            <label for="inputSurname" class="col-sm-2 control-label">${course_name}</label>

                            <div class="col-sm-10">
                                <input type="text" name="name" pattern="${textPattern}" class="form-control"
                                       id="inputSurname" placeholder="${course_name}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lector" class="col-sm-2 control-label">${course_lector}</label>

                            <div class="col-sm-10">
                                <select id="lector" name="lector" class="form-control">
                                    <c:forEach var="teacher" items="${requestScope.teacher_list}">
                                        <option value="${teacher.id}" >${teacher.surname} ${teacher.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputStartDate" class="col-sm-2 control-label">${course_start_date}</label>

                            <div class="col-sm-10">
                                <input type="date" name="start_date" class="form-control" onclick="startDateInit('inputStartDate','inputEndDate',null)" onchange="endDateInit('inputStartDate','inputEndDate')"
                                       id="inputStartDate" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEndDate" class="col-sm-2 control-label">${course_end_date}</label>

                            <div class="col-sm-10">
                                <input type="date" disabled="disabled" name="end_date" class="form-control"
                                       id="inputEndDate" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="submit" style="width: 100%" class="btn btn-primary"
                                       value="${course_add_label}"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-offset-0 col-sm-12">
                        <button type="button"onclick="javascript:history.back();" style="width: 100%" class="btn btn-primary">${back}</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
