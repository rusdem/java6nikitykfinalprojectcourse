<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@include file="header.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Registration</title>
    <link rel='stylesheet' href='css_js/css/bootstrap.min.css' type='text/css' media='all'>
    <fmt:setLocale value="${sessionScope.localization}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="authentication.login_label" var="login_label"/>
    <fmt:message bundle="${loc}" key="authentication.password_label" var="password_label"/>
    <fmt:message bundle="${loc}" key="registration_button" var="registration_button"/>
    <fmt:message bundle="${loc}" key="redirect.to_index" var="to_index"/>
    <fmt:message bundle="${loc}" key="registration_surname_label" var="registration_surname_label"/>
    <fmt:message bundle="${loc}" key="registration_name_label" var="registration_name_label"/>
    <fmt:message bundle="${loc}" key="registration_education_label" var="registration_education_label"/>
    <fmt:message bundle="${loc}" key="data_error" var="registration_data_error_label"/>
    <fmt:message bundle="${loc}" key="login_error" var="registration_login_error_label"/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-10 col-sm-offset-1" style="background-color: white;opacity: 0.9">
            <br/>
                <c:if test="${requestScope.data_error!=null}">
                    <h1 style="text-align: center; color: red">${registration_data_error_label}</h1>
                </c:if>
                <c:if test="${requestScope.login_error!=null}">
                    <h1 style="text-align: center; color: red">${registration_login_error_label}</h1>
                </c:if>
            <form action="controller" class="form-horizontal" method="post">
                <input type="hidden" name="command" value="student_registration_command">

                <div class="form-group">
                    <label for="inputLogin" class="col-sm-2 control-label">${login_label}</label>

                    <div class="col-sm-10">
                        <input type="text" name="login" pattern="^[A-Za-z-_0-9]{4,20}$" class="form-control"
                               id="inputLogin" placeholder="${login_label}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword" class="col-sm-2 control-label">${password_label}</label>

                    <div class="col-sm-10">
                        <input type="password" name="password" pattern="^[A-Za-z-_0-9]{4,20}$" class="form-control"
                               id="inputPassword" placeholder="${password_label}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputSurname" class="col-sm-2 control-label">${registration_surname_label}</label>

                    <div class="col-sm-10">
                        <input type="text" name="surname" pattern="^[А-Яа-яA-Za-z-]{1,40}$" class="form-control"
                               id="inputSurname" placeholder="${registration_surname_label}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">${registration_name_label}</label>

                    <div class="col-sm-10">
                        <input type="text" name="name" pattern="^[А-Яа-яA-Za-z-]{1,40}$" class="form-control"
                               id="inputName" placeholder="${registration_name_label}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEducation" class="col-sm-2 control-label">${registration_education_label}</label>

                    <div class="col-sm-10">
                        <input type="text" name="education" pattern="^[A-Za-z-А-Яа-я0-9 \.]+$" class="form-control"
                               id="inputEducation" placeholder="${registration_education_label}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button style="width: 100%" type="submit"
                                class="btn btn-primary">${registration_button}</button>
                    </div>
                </div>
            </form>

            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="button"onclick="javascript:history.back();" style="width: 100%" class="btn btn-primary">${to_index}</button>
                    </div>
                </div>
            </div>
            <br/>
        </div>
    </div>
</div>
</body>
</html>
