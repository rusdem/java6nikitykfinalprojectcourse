<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@include file="header.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel='stylesheet' href='css_js/css/bootstrap.min.css' type='text/css' media='all'>
    <fmt:setLocale value="${sessionScope.localization}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="education_label" var="education_label"/>
    <fmt:message bundle="${loc}" key="search" var="search"/>
    <fmt:message bundle="${loc}" key="name_label" var="name"/>
    <fmt:message bundle="${loc}" key="surname_label" var="surname"/>
    <fmt:message bundle="${loc}" key="education_label" var="education"/>
    <fmt:message bundle="${loc}" key="mark" var="mark"/>
    <fmt:message bundle="${loc}" key="teacher's_note" var="note"/>
    <fmt:message bundle="${loc}" key="save" var="save"/>
    <fmt:message bundle="${loc}" key="back" var="back"/>
    <fmt:message bundle="${loc}" key="data_error" var="data_error"/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <br/>

        <div class="col-xs-10 col-sm-offset-1" style="background-color: white;opacity: 0.9">
            <br/>
            <form action="controller" method="post" class="form-horizontal">
                <input type="hidden" name="command" value="teacher_course_filter_command">
                <input type="hidden" name="id" value="${sessionScope.user.id}">

                <div class="col-sm-12">
                    <div class="input-group">
                        <select name="course_id" id="course_id" class="form-control">
                            <c:forEach var="option" items="${requestScope.options}">
                                <option value="${option.id}">${option.name} (${option.startDate}/${option.endDate})
                                </option>
                            </c:forEach>
                        </select>
                        <span class="input-group-btn">
                            <input type="submit" style="width: 100%"
                                   class="btn btn-primary" value="${search}"/>
                        </span>
                    </div>
                </div>
            </form>
            <br/>
            <br/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title" style="text-align: center">${requestScope.course_name}</h3>
                </div>
                <div class="panel-body">
                    <c:if test="${requestScope.data_error!=null}">
                        <div class="alert alert-warning alert-dismissable">
                            <strong>${data_error}</strong>
                        </div>
                    </c:if>
                    <table class="table table-hover">
                        <tr>
                            <td>
                                <h4>${surname}/${name}</h4>
                            </td>
                            <td>
                                <h4>${education_label}</h4>
                            </td>
                            <td>
                                <h4>${mark}</h4>
                            </td>
                            <td>
                                <h4>${note}</h4>
                            </td>
                            <td>
                                <h4>${save}</h4>
                            </td>
                        </tr>
                        <c:forEach var="inputData" items="${requestScope.teacher_courses_list}">
                            <tr>
                                <form action="controller" method="post">
                                    <input type="hidden" name="command" value="student_on_course_update_command">
                                    <input type="hidden" name="id" value="${inputData[0].id}">
                                    <td>
                                        <h5>${inputData[1]}</h5>
                                    </td>
                                    <td>
                                        <h5>${inputData[2]}</h5>
                                    <td>
                                        <input type="text" pattern="^10|[0-9]$" name="mark" class="form-control" value="${inputData[0].mark}" required>
                                    </td>
                                    <td>
                                        <input type="text" name="note" class="form-control" value="${inputData[0].note}" required>
                                    </td>
                                    <td>
                                        <input type="submit" style="width: 100%"
                                               class="btn btn-primary" value="${save}"/>
                                    </td>
                                </form>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-offset-0 col-sm-12">
                        <form id="to_main_form" action="controller" method="post">
                            <input type="hidden" name="command" value="to_main_page_command">
                            <button type="submit" style="width: 100%" class="btn btn-primary">${back}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
