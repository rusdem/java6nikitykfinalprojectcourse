<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@include file="header.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel='stylesheet' href='css_js/css/bootstrap.min.css' type='text/css' media='all'>
    <fmt:setLocale value="${sessionScope.localization}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="cancel" var="cancel_button"/>
    <fmt:message bundle="${loc}" key="save" var="save_button"/>
    <fmt:message bundle="${loc}" key="registration_surname_label" var="surname_label"/>
    <fmt:message bundle="${loc}" key="registration_name_label" var="name_label"/>
    <fmt:message bundle="${loc}" key="registration_graduation_label" var="graduation_label"/>
    <fmt:message bundle="${loc}" key="user_info" var="user_info"/>
    <fmt:message bundle="${loc}" key="data_error" var="data_error"/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-10 col-sm-offset-1" style="background-color: white;opacity: 0.9">
            <br/>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title" style="text-align: center">${user_info}</h3>
                </div>
                <div class="panel-body">
                    <c:if test="${requestScope.data_error!=null}">
                        <div class="alert alert-warning alert-dismissable">
                            <strong>${data_error}</strong>
                        </div>
                    </c:if>
                    <form action="controller" class="form-horizontal" method="post">
                        <input type="hidden" name="command" value="teacher_update_command">
                        <input type="hidden" name="id" value="${sessionScope.user.id}">

                        <div class="form-group">
                            <label for="inputSurname" class="col-sm-2 control-label">${surname_label}</label>

                            <div class="col-sm-10">
                                <input type="text" name="surname" pattern="^[А-Яа-яA-Za-z-]{1,40}$" class="form-control"
                                       id="inputSurname" placeholder="${surname_label}"
                                       value="${sessionScope.user.surname}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-sm-2 control-label">${name_label}</label>

                            <div class="col-sm-10">
                                <input type="text" name="name" pattern="^[А-Яа-яA-Za-z-]{1,40}$" class="form-control"
                                       id="inputName" placeholder="${name_label}" value="${sessionScope.user.name}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputGraduation" class="col-sm-2 control-label">${graduation_label}</label>

                            <div class="col-sm-10">
                                <input type="text" name="graduation" pattern="^[A-Za-z-А-Яа-я0-9 \.]+$"
                                       class="form-control"
                                       id="inputGraduation" placeholder="${graduation_label}"
                                       value="${sessionScope.user.graduation}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="submit" style="width: 100%" class="btn btn-primary"
                                       value="${save_button}"/>
                            </div>
                        </div>
                    </form>

                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-offset-0 col-sm-12">
                                <button type="button"onclick="javascript:history.back();" style="width: 100%" class="btn btn-primary">${cancel_button}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
