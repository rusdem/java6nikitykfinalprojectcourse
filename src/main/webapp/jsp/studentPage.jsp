<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@include file="header.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Welcome, Student</title>
    <link rel='stylesheet' href='css_js/css/bootstrap.min.css' type='text/css' media='all'>
    <fmt:setLocale value="${sessionScope.localization}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="education_label" var="education_label"/>
    <fmt:message bundle="${loc}" key="name_label" var="name_label"/>
    <fmt:message bundle="${loc}" key="surname_label" var="surname_label"/>
    <fmt:message bundle="${loc}" key="page.choose_course" var="choose_course_button"/>
    <fmt:message bundle="${loc}" key="update_button" var="update_button"/>
    <fmt:message bundle="${loc}" key="user_info" var="user_info"/>
    <fmt:message bundle="${loc}" key="student_attend_courses" var="attend_courses"/>
    <fmt:message bundle="${loc}" key="course_name_label" var="course_name"/>
    <fmt:message bundle="${loc}" key="course_lector_label" var="course_lector"/>
    <fmt:message bundle="${loc}" key="course_start_date" var="course_start_date"/>
    <fmt:message bundle="${loc}" key="course_end_date" var="course_end_date"/>
    <fmt:message bundle="${loc}" key="leave_course" var="leave_course"/>
    <fmt:message bundle="${loc}" key="mark" var="mark"/>
    <fmt:message bundle="${loc}" key="teacher's_note" var="teachers_note"/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-10 col-sm-offset-1" style="background-color: white;opacity: 0.9">
            <br/>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title" style="text-align: center">${user_info}</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="Surname" class="col-sm-2 control-label">${surname_label}</label>

                        <div class="col-sm-10">
                            <p class="lead" id="Surname">${sessionScope.user.surname}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Name" class="col-sm-2 control-label">${name_label}</label>

                        <div class="col-sm-10">
                            <p class="lead" id="Name">${sessionScope.user.name}</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="Education" class="col-sm-2 control-label">${education_label}</label>

                        <div class="col-sm-10">
                            <p class="lead" id="Education">${sessionScope.user.education}</p>
                        </div>
                    </div>
                    <c:if test="${sessionScope.user.id == sessionScope.authorized_id}">
                        <form action="controller" class="form-horizontal" method="post">
                            <input type="hidden" name="command" value="redirect_to_student_edit_command">

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <input type="submit" style="width: 100%" class="btn btn-default"
                                           value="${update_button}"/>
                                </div>
                            </div>
                        </form>
                    </c:if>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title" style="text-align: center">${attend_courses}</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <tr>
                            <td>
                                <h4>${course_name}</h4>
                            </td>
                            <td>
                                <h4>${course_start_date}</h4>
                            </td>
                            <td>
                                <h4>${course_end_date}</h4>
                            </td>
                            <td>
                                <h4>${mark}</h4>
                            </td>
                            <td>
                                <h4>${course_lector}</h4>
                            </td>
                            <td>
                                <h4>${teachers_note}</h4>
                            </td>
                            <td>
                                <h4>${leave_course}</h4>
                            </td>
                        </tr>
                        <c:forEach var="inputData" items="${requestScope.student_on_course_list}">
                            <tr>
                                <form action="controller" method="post">
                                    <input type="hidden" name="command"
                                           value="student_on_course_delete_command">
                                    <input type="hidden" name="id" value="${inputData[0].id}">
                                    <td>
                                        <h5>${inputData[1].name}</h5>
                                    </td>
                                    <td>
                                        <h5>${inputData[1].startDate}</h5>
                                    <td>
                                        <h5>${inputData[1].endDate}</h5>
                                    </td>
                                    <td>
                                        <h5>${inputData[0].mark}</h5>
                                    </td>
                                    <td>
                                        <h5>${inputData[2]}</h5>
                                    </td>
                                    <td>
                                        <h5>${inputData[0].note}</h5>
                                    </td>
                                    <td>
                                        <input type="submit" style="width: 100%"
                                               class="btn btn-danger" value="${leave_course}"/>
                                    </td>
                                </form>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
            <form action="controller" class="form-horizontal" method="post">
                <input type="hidden" name="command" value="to_student_on_course_add_command">
                <input type="hidden" name="id" value="${sessionScope.user.id}">

                <div class="form-group">
                    <div class="col-sm-offset-0 col-sm-12">
                        <input type="submit" style="width: 100%" class="btn btn-primary"
                               value="${choose_course_button}"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
