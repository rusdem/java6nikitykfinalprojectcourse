<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@include file="header.jsp" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.localization}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="admin_registration_label" var="admin_registration"/>
    <fmt:message bundle="${loc}" key="teacher_registration_label" var="teacher_registration"/>
    <fmt:message bundle="${loc}" key="course_add_label" var="course_add"/>
    <fmt:message bundle="${loc}" key="course_edit_label" var="course_edit"/>
    <fmt:message bundle="${loc}" key="course_actions" var="course_actions"/>
    <fmt:message bundle="${loc}" key="authentication.login_label" var="login_label"/>
    <fmt:message bundle="${loc}" key="authentication.password_label" var="password_label"/>
    <fmt:message bundle="${loc}" key="data_error" var="data_error"/>
    <fmt:message bundle="${loc}" key="login_error" var="login_error"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Welcome, Admin</title>
    <link rel='stylesheet' href='css_js/css/bootstrap.min.css' type='text/css' media='all'>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-10 col-sm-offset-1" style="background-color: white;opacity: 0.9">
            <br/>
            <c:if test="${requestScope.data_error!=null}">
                <div class="alert alert-warning alert-dismissable">
                    <strong>${data_error}</strong>
                </div>
            </c:if>
            <c:if test="${requestScope.login_error!=null}">
                <div class="alert alert-warning alert-dismissable">
                    <strong>${login_error}</strong>
                </div>
            </c:if>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">${admin_registration}</h3>
                </div>
                <div class="panel-body">
                    <form action="controller" class="form-horizontal" method="post">
                        <input type="hidden" name="command" value="admin_registration_command"/>

                        <div class="form-group">
                            <label for="adminLogin" class="col-sm-2 control-label">${login_label}</label>

                            <div class="col-sm-10">
                                <input type="text" name="login" pattern="^[A-Za-z-_0-9]{4,20}$" class="form-control"
                                       id="adminLogin" placeholder="${login_label}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="adminPassword" class="col-sm-2 control-label">${password_label}</label>

                            <div class="col-sm-10">
                                <input type="password" name="password" pattern="^[A-Za-z-_0-9]{4,20}$"
                                       class="form-control"
                                       id="adminPassword" placeholder="${password_label}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="submit" style="width: 100%" class="btn btn-primary"
                                       value="${admin_registration}"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br/>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">${teacher_registration}</h3>
                </div>
                <div class="panel-body">
                    <form action="controller" class="form-horizontal" method="post">
                        <input type="hidden" name="command" value="teacher_registration_command"/>

                        <div class="form-group">
                            <label for="teacherLogin" class="col-sm-2 control-label">${login_label}</label>

                            <div class="col-sm-10">
                                <input type="text" name="login" pattern="^[A-Za-z-_0-9]{4,20}$" class="form-control"
                                       id="teacherLogin" placeholder="${login_label}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="teacherPassword" class="col-sm-2 control-label">${password_label}</label>

                            <div class="col-sm-10">
                                <input type="password" name="password" pattern="^[A-Za-z-_0-9]{4,20}$"
                                       class="form-control"
                                       id="teacherPassword" placeholder="${password_label}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="submit" style="width: 100%" class="btn btn-primary"
                                       value="${teacher_registration}"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">${course_actions}</h3>
                </div>
                <div class="panel-body">
                    <form action="controller" class="form-horizontal" method="post">
                        <input type="hidden" name="command" value="to_course_add_command"/>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="submit" style="width: 100%" class="btn btn-primary"
                                       value="${course_add}"/>
                            </div>
                        </div>
                    </form>
                    <form action="controller" class="form-horizontal" method="post">
                        <input type="hidden" name="command" value="redirect_to_course_edit_command"/>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="submit" style="width: 100%" class="btn btn-primary"
                                       value="${course_edit}"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
