<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="selector" uri="/WEB-INF/tld/taglib.tld" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel='stylesheet' href='css_js/css/bootstrap.min.css' type='text/css' media='all'>
    <script src='css_js/js/myfunc.js'></script>
    <fmt:setLocale value="${sessionScope.localization}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="guest_label" var="guest_label"/>
    <fmt:message bundle="${loc}" key="hello_label" var="hello_label"/>
    <fmt:message bundle="${loc}" key="logout_button" var="logout_button"/>
    <fmt:message bundle="${loc}" key="welcome_label" var="welcome_label"/>
    <fmt:message bundle="${loc}" key="local.en_button" var="en_button"/>
    <fmt:message bundle="${loc}" key="local.ru_button" var="ru_button"/>
    <fmt:message bundle="${loc}" key="redirect.to_index" var="main"/>
    <fmt:message bundle="${loc}" key="local.lang_change" var="set_lang"/>
</head>
<body background="image/background.jpg" style="background-size: 100%;background-position: center">
<form id="to_main_form" action="controller" method="post">
    <input type="hidden" name="command" value="to_main_page_command">
</form>

<form id="logout_form" action="controller" method="post">
    <input type="hidden" name="command" value="logout_command">
</form>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-10 col-sm-offset-1" style="background-color: white;opacity: 0.9">
            <table style="width: 100%">
                <tr>
                    <td style="width: 82%">
                        <h1 style="text-align: center;margin-left: 25%;">${welcome_label}</h1>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <c:if test="${sessionScope.role!=null}">
                                            <h3 class="panel-title"
                                                style="text-align: center">${hello_label}, ${role}</h3>
                                        </c:if>
                                        <c:if test="${sessionScope.role==null}">
                                            <h3 class="panel-title"
                                                style="text-align: center">${hello_label}, ${guest_label}</h3>
                                        </c:if>
                                    </div>
                                    <div class="panel-body">
                                        <div class="btn-group-vertical" style="width: 100%">
                                            <c:if test="${sessionScope.role!=null}">
                                                <button type="submit" class="btn btn-danger" form="logout_form"
                                                        style="width: 100%">${logout_button}</button>
                                            </c:if>
                                            <button type="submit" class="btn btn-primary" form="to_main_form"
                                                    style="width: 100%">${main}</button>
                                            <select name="local" id="local" class="form-control"
                                                    onchange="changeLang('local')">
                                                <option>${set_lang}</option>
                                                <option value="en">${en_button}</option>
                                                <option value="ru">${ru_button}</option>
                                            </select>
                                        </div>
                                    </div>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<br/>
</body>
</html>
