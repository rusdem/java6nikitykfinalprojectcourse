<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@include file="header.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Welcome, Admin</title>
    <link rel='stylesheet' href='css_js/css/bootstrap.min.css' type='text/css' media='all'>
    <fmt:setLocale value="${sessionScope.localization}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="redirect.to_index" var="to_index"/>
    <fmt:message bundle="${loc}" key="back" var="back"/>
    <fmt:message bundle="${loc}" key="error.command" var="unknown_error"/>
    <fmt:message bundle="${loc}" key="error.no_access_action" var="access_error"/>
    <fmt:message bundle="${loc}" key="error.database_offline" var="database_error"/>
    <fmt:message bundle="${loc}" key="error.command" var="command_error"/>
    <fmt:message bundle="${loc}" key="error.null_pointer" var="null_error"/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <br/>
        <div class="col-xs-10 col-sm-offset-1" style="background-color: white;opacity: 0.9">
            <br/>

            <div class="alert alert-warning alert-dismissable">
                <h2 style="color: red; text-align: center">
                    <c:if test="${requestScope.access_error!=null}">
                        ${access_error}
                    </c:if>
                    <c:if test="${requestScope.command_error!=null}">
                        ${command_error}
                    </c:if>
                    <c:if test="${requestScope.null_pointer_error!=null}">
                        ${null_error}
                    </c:if>
                    <c:if test="${requestScope.database_error!=null}">
                        ${database_error}
                    </c:if>
                </h2>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-offset-0 col-sm-12">
                        <button type="button" onclick="javascript:history.back();" style="width: 100%"
                                class="btn btn-primary">${back}</button>
                    </div>
                </div>
            </div>
            <form action="controller" method="post" class="form-horizontal">
                <input type="hidden" name="command" value="to_main_page_command"/>

                <div class="form-group">
                    <div class="col-sm-offset-0 col-sm-12">
                        <input type="submit" style="width: 100%" class="btn btn-primary" value="${to_index}"/>

                        <h1></h1>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>
</body>
</html>
