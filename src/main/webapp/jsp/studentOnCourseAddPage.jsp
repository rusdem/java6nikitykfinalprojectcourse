<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@include file="header.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel='stylesheet' href='css_js/css/bootstrap.min.css' type='text/css' media='all'>
    <script src='css_js/js/myfunc.js'></script>
    <script src="css_js/js/jquery-1.3.2.min.js"></script>
    <fmt:setLocale value="${sessionScope.localization}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="back" var="back"/>
    <fmt:message bundle="${loc}" key="course_name_label" var="course_name"/>
    <fmt:message bundle="${loc}" key="course_lector_label" var="course_lector"/>
    <fmt:message bundle="${loc}" key="course_start_date" var="course_start_date"/>
    <fmt:message bundle="${loc}" key="course_end_date" var="course_end_date"/>
    <fmt:message bundle="${loc}" key="check_in_course" var="check_in"/>
    <fmt:message bundle="${loc}" key="page.choose_course" var="choose_course"/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-10 col-sm-offset-1" style="background-color: white;opacity: 0.9">
            <br/>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title" style="text-align: center">${choose_course}</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <tr>
                            <td>
                                <h4>${course_name}</h4>
                            </td>
                            <td>
                                <h4>${course_start_date}</h4>
                            </td>
                            <td>
                                <h4>${course_end_date}</h4>
                            </td>
                            <td>
                                <h4>${course_lector}</h4>
                            </td>
                            <td>
                                <h4>${check_in}</h4>
                            </td>
                        </tr>
                        <c:forEach var="inputData" items="${requestScope.student_not_on_course_list}">
                            <tr>
                                <form action="controller" method="post">
                                    <input type="hidden" name="command"
                                           value="student_on_course_add_command">
                                    <input type="hidden" name="id" value="${sessionScope.user.id}">
                                    <input type="hidden" name="course_id" value="${inputData[0].id}">
                                    <td>
                                        <h5>${inputData[0].name}</h5>
                                    </td>
                                    <td>
                                        <h5>${inputData[0].startDate}</h5>
                                    <td>
                                        <h5>${inputData[0].endDate}</h5>
                                    </td>
                                    <td>
                                        <h5>${inputData[1]}</h5>
                                    </td>
                                    <td>
                                        <input type="submit" style="width: 100%"
                                               class="btn btn-primary" value="${check_in}"/>
                                    </td>
                                </form>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-offset-0 col-sm-12">
                        <form id="to_main_form" action="controller" method="post">
                            <input type="hidden" name="command" value="to_main_page_command">
                            <button type="submit" style="width: 100%" class="btn btn-primary">${back}</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>
