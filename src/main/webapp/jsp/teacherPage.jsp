<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@include file="header.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Welcome, Teacher</title>
    <link rel='stylesheet' href='css_js/css/bootstrap.min.css' type='text/css' media='all'>
    <fmt:setLocale value="${sessionScope.localization}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="graduation_label" var="graduation_label"/>
    <fmt:message bundle="${loc}" key="name_label" var="name_label"/>
    <fmt:message bundle="${loc}" key="surname_label" var="surname_label"/>
    <fmt:message bundle="${loc}" key="page.choose_course" var="choose_course_button"/>
    <fmt:message bundle="${loc}" key="update_button" var="update_button"/>
    <fmt:message bundle="${loc}" key="user_info" var="user_info"/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-10 col-sm-offset-1" style="background-color: white;opacity: 0.9">
            <br/>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title" style="text-align: center">${user_info}</h3>
                </div>
                <div class="panel-body">
                    <table>
                        <tr>
                            <td style="width: 32%">
                                <h5><strong>${surname_label}</strong></h5>
                            </td>
                            <td>
                                <h5>${sessionScope.user.surname}</h5>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32%">
                                <h5><strong>${name_label}</strong></h5>
                            </td>
                            <td>
                                <h5>${sessionScope.user.name}</h5>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32%">
                                <h5><strong>${graduation_label}</strong></h5>
                            </td>
                            <td>
                                <h5>${sessionScope.user.graduation}</h5>
                            </td>
                        </tr>
                    </table>
                    <c:if test="${sessionScope.user.id == sessionScope.authorized_id}">
                        <form action="controller" class="form-horizontal" method="post">
                            <input type="hidden" name="command" value="redirect_to_teacher_edit_command">

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <input type="submit" style="width: 100%" class="btn btn-default"
                                           value="${update_button}"/>
                                </div>
                            </div>
                        </form>
                    </c:if>
                    <form action="controller" class="form-horizontal" method="post">
                        <input type="hidden" name="command" value="to_teacher_edit_course_command">
                        <input type="hidden" name="id" value="${sessionScope.user.id}">

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="submit" style="width: 100%" class="btn btn-primary"
                                       value="${choose_course_button}"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
