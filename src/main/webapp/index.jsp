<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@include file="jsp/header.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Index</title>
    <link rel='stylesheet' href='css_js/css/bootstrap.min.css' type='text/css' media='all'>
    <fmt:setLocale value="${sessionScope.localization}"/>
    <fmt:setBundle basename="localization.local" var="loc"/>
    <fmt:message bundle="${loc}" key="authentication.login_label" var="login_label"/>
    <fmt:message bundle="${loc}" key="authentication.password_label" var="password_label"/>
    <fmt:message bundle="${loc}" key="authentication_button" var="authentication_button"/>
    <fmt:message bundle="${loc}" key="authentication_error_label" var="authorization_error_label"/>
    <fmt:message bundle="${loc}" key="registration_button" var="registration_button"/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-10 col-sm-offset-1" style="background-color: white;opacity: 0.9">
            <br/>
            <c:if test="${requestScope.authorization_error!=null}">
                <h1 style="text-align: center; color: red">${authorization_error_label}</h1>
            </c:if>
            <form class="form-horizontal" action="controller" method="post">
                <input type="hidden" name="command" value="authentication_command"/>

                <div class="form-group">
                    <label for="inputLogin" class="col-sm-2 control-label">${login_label}</label>

                    <div class="col-sm-10">
                        <input type="text" name="login" pattern="^[A-Za-z-_0-9]{4,20}$" class="form-control"
                               id="inputLogin" placeholder="${login_label}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword" class="col-sm-2 control-label">${password_label}</label>

                    <div class="col-sm-10">
                        <input type="password" name="password" pattern="^[A-Za-z-_0-9]{4,20}$" class="form-control"
                               id="inputPassword" placeholder="${password_label}"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button style="width: 100%" type="submit"
                                class="btn btn-primary">${authentication_button}</button>
                    </div>
                </div>
            </form>

            <form class="form-horizontal" action="controller" method="post">
                <input type="hidden" name="command" value="redirect_to_registration_command"/>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" style="width: 100%" class="btn btn-primary"
                               value="${registration_button}"/>
                    </div>
                </div>
            </form>
            <br/>
        </div>
    </div>
</div>
</body>
</html>
