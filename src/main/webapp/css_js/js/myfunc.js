function startDateInit(startDateElementId, endDateElementId, lowDateBorder) {
    var d;
    if(lowDateBorder!=null){
        d = new Date(lowDateBorder);
    }
    else
    {
        d = new Date();
    }
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (month < 10) {
        month = "0" + month;
    }
    if (day < 10) {
        day = "0" + day;
    }
    var minDate = "" + year + "-" + month + "-" + day;
    var startDate = document.getElementById(startDateElementId);
    startDate.setAttribute('min', minDate);
    document.getElementById(endDateElementId).removeAttribute("disabled");
}

function endDateInit(startDateElementId, endDateElementId) {
    document.getElementById(endDateElementId).setAttribute('min', document.getElementById(startDateElementId).value);
}

function getXmlHttp() {
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// ��� ��� IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// ��� ��� IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    return xmlhttp;
}

function changeLang(locElementName) {
    var local = document.getElementById(locElementName).value;
    var xmlhttp = getXmlHttp(); // ������ ������ XMLHTTP
    xmlhttp.open("POST", "controller", false); // ��������� ����������� ����������
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); // ���������� ���������
    xmlhttp.send("local=" + local + "&command=change_locale_command"); // ���������� POST-������
    location.reload();
}

function deleteCourse(ID, innerBtn){
    var course = {
        id: ID
    };
    var request={};
    request.command = 'course_delete_command';
    request.data = JSON.stringify(course);
    $.ajax({
        url: "controller",
        type: "POST",
        dataType: "json",
        data: JSON.stringify(request),
        contentType: "application/json",
        mimeType: "application/json",
        success: function(data){
            innerBtn.parentElement.parentElement.remove();
        },
        error: function(){
            alert('\u00d7');
        }
    })
}
function deleteStudentOnCourse(ID, innerBtn){
    var studentOnCourse = {
        id: ID
    };
    var request={};
    request.command = 'student_on_course_delete_command';
    request.data = JSON.stringify(studentOnCourse);
    $.ajax({
        url: "controller",
        type: "POST",
        dataType: "json",
        data: JSON.stringify(request),
        contentType: "application/json",
        mimeType: "application/json",
        success: function(data){
            innerBtn.parentElement.parentElement.remove();
        },
        error: function(){
            alert('\u00d7');
        }
    })
}